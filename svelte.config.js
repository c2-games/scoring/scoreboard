import adapter from '@sveltejs/adapter-auto';
import { vitePreprocess } from '@sveltejs/kit/vite';
import path from 'path';

// eslint-disable-next-line no-undef
const SANDBOX_BUILD = !!process.env.SANDBOX_BUILD;

/** @type {import("@sveltejs/kit").Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [vitePreprocess()],
  kit: {
    adapter: adapter(),
    files: {
      routes: SANDBOX_BUILD ? 'src/sandbox' : 'src/routes',
    },
    alias: {
      $houdini: path.resolve('.', '$houdini'),
      '@models/*': 'src/models',
      '@assets/*': 'src/assets',
      '@components/*': 'src/components',
      '@queries/*': 'src/queries',
      '@auth/*': 'src/auth',
      '@utils/*': 'src/utils',
      '@stores': 'src/stores.ts',
      '@db': 'src/db.ts',
      '@env': 'src/env.ts',
    },
  },
};

export default config;
