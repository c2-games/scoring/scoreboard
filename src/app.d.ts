// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
import type { EventRole } from '@auth/models';

declare namespace App {
  // interface Locals {}
  // interface PageData {}
  // interface Error {}
  // interface Platform {}
  // houdini session shared across all queries
  interface Session {
    token?: string | null;
    roles?: string[] | null;
  }

  // houdini metadata passed to each query
  interface Metadata {
    // used by houdini client.ts
    role?: EventRole;
    roleDirective?: RoleDirectiveArg;
  }

  interface Locals {
    token?: string;
    roles?: EventRole[];
  }
}
