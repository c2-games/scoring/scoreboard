import { createClient } from 'graphql-ws';
import { keycloak } from '@stores';
import Env from './env';
import type { Sink, Client } from 'graphql-ws';
import type { NullableEventRole } from '@auth/models';
import type { OidcManager } from '@auth/keycloak';

/**
 * GQL Subscribe Payload. We had to make our own because the actual GQL payload has its attributes marked "readonly"
 */
type SubscribePayload = {
  operationName?: string | null;
  query: string;
  variables?: Record<string, unknown> | null;
  extensions?: Record<string, unknown> | null;
};

/**
 * Options provided for a Query. Mostly the same as SubscribePayload
 * queryRole provided as an alias of `role`
 */
export type QueryOptions = {
  extensions?: Record<string, unknown> | null;
  variables?: Record<string, unknown> | null;
  operationName?: string;
  role?: NullableEventRole;
  queryRole?: NullableEventRole; // alias of role
};

export type QueryHeaders = {
  'content-type'?: string;
  'X-Hasura-Role'?: string;
  'x-hasura-admin-secret'?: string;
  Authorization?: string;
};

class _GqlInterface {
  // just store a map of "role": Client to enable re-using sockets.
  // we might need special handling to re-enable subscription sinks when a socket is reloaded
  // For example, when the keycloak token expires - but in testing, it seems like the socket automatically reloads
  sockets: Record<string, Client>;
  keycloak?: OidcManager;
  private queryCount = 0;

  constructor() {
    this.sockets = {};
  }

  initialize() {
    keycloak.subscribe((value) => (this.keycloak = value));
    console.debug('GraphQL: Initialized', this);
  }

  getGqlHeaders({
    role = undefined,
    adminSecretKey = undefined,
  }: { role?: string | null; adminSecretKey?: string } = {}) {
    const headers: QueryHeaders = { 'content-type': 'application/json' };
    // Append keycloak token if it's available
    // Set JWT Token and allow overriding role
    if (role) {
      headers['X-Hasura-Role'] = role;
      // only send the keycloak token if a role is specified,
      // otherwise leave it out for anonymous operations
      if (!this.keycloak?.token) {
        console.warn('a role was specified but no JWT is available!');
      } else {
        headers.Authorization = `Bearer ${this.keycloak.token}`;
      }
    }

    // Set Admin Key
    if (adminSecretKey) {
      headers['x-hasura-admin-secret'] = adminSecretKey;
    }

    return headers;
  }

  getSocket(role: string | null | undefined = undefined) {
    /*
     * Get a GraphQL websocket with the appropriate headers defined:
     *  - If a role is NOT defined, the socket will be anonymous (no auth token)
     *  - If a role IS defined, the role will be sent in a header and a JWT included if available
     * You should **NEVER** send role='anonymous' to this function
     */
    // store a null or undefined role as anonymous
    const storageRole = role || 'anonymous';

    // create and save socket if it doesn't exist
    if (!this.sockets[storageRole]) {
      console.debug(`creating new gql client for ${storageRole}`);
      this.sockets[storageRole] = createClient({
        url: Env.GqlPublicUri.replace('http', 'ws'),
        connectionParams: () => {
          // This is a function so that we get an updated auth token when it expires
          return {
            headers: this.getGqlHeaders({ role }),
          };
        },
      });
    }
    // Return a cached socket based on role
    return this.sockets[storageRole];
  }

  /**
   * Try to execute a query, and return true if successful
   */
  async tryQuery(query: string, queryOptions: QueryOptions = {}): Promise<boolean> {
    try {
      await this.query(query, queryOptions);
    } catch (err) {
      console.error(err);
      return false;
    }

    return true;
  }

  async query<T = unknown>(
    query: string,
    { extensions, variables, operationName, role, queryRole }: QueryOptions = {},
  ): Promise<T> {
    /*
     * Make a one-time query against the DB.
     * query (string): Query to execute as a string. May contain fragments
     * payloadOptions (object): See C2GamesScoreboard.db.subscribe for payload options.
     */
    // wow how ugly...
    return await new Promise((resolve, reject) => {
      if (queryRole && !role) role = queryRole; // queryRole is an alias of Role

      // Default to the highest possible role, since all roles inherit from anonymous
      if (!role) role = this.keycloak?.getHighestPrivilegeRole();

      // safety check for a subscription
      if (query.trim().startsWith('subscription')) {
        return reject("cowardly refusing to execute a query starting with 'subscription'");
      }

      let response: null | { data: T } = null; // we should only ever get `next` once
      const queryId = (this.queryCount += 1);
      const sink = {
        complete() {
          console.debug(`query ${queryId} complete!`);
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          resolve(response!.data);
        },
        error(err: unknown) {
          reject(err);
        },
        next(val: { data: T }) {
          if (response !== null) {
            console.warn(`Query ${queryId}: received extra data on a query operation, how did this happen?`);
          }
          response = val;
        },
      };

      // const payload = {query: graph.buildQuery(query)} // we used to build queries with fragments
      const payload: SubscribePayload = { query };
      console.debug(`${queryId} (${role}): ${query}`);
      // Extra supported parameters
      if (extensions) payload.extensions = extensions;
      if (variables) {
        console.debug(`${queryId} VARS`, variables);
        payload.variables = variables;
      }
      if (operationName) payload.operationName = operationName;
      // We send a query request through the subscription library, but because we don't
      // specify the `subscription` keyword, it will return the data once and complete immediately.
      // Then we resolve the Promise we created with the result from `next()` once the connection
      // completes, and return the data received.
      this.getSocket(role).subscribe(payload, sink);
    });
  }

  subscribeSimple<T>(query: string, cb: (value: T) => void, queryOptions: QueryOptions = {}) {
    const SimpleSubscriptionSink = {
      complete: () => console.debug('simple sink complete for query', query),
      error: (err: unknown) => console.error(err),
      next: (value: { data: T }) => cb(value.data),
    };
    return this.subscribe(query, SimpleSubscriptionSink, queryOptions);
  }

  subscribe(
    query: string,
    sink: Sink,
    { extensions, variables, operationName, role, queryRole }: QueryOptions = {},
  ): () => void {
    /*
     * Subscribe to changes to the database using a sink interface
     * query (string): String containing subscription query. May contain fragments.
     *                 Should start with `subscription {`
     * sink (object): Sink Interface containing callbacks for complete, error and next events
     *                https://github.com/enisdenjo/graphql-ws/blob/master/docs/interfaces/common.Sink.md
     * SubscribeOptions (object): Additional options to include in payload
     * https://github.com/enisdenjo/graphql-ws/blob/master/docs/interfaces/common.SubscribePayload.md
     */
    if (queryRole && !role) role = queryRole; // queryRole is an alias of Role

    // Default to the highest possible role, since all roles inherit from anonymous
    if (!role) role = this.keycloak?.getHighestPrivilegeRole();

    const queryId = (this.queryCount += 1);
    if (!query.trim().startsWith('subscription')) {
      console.warn(`subscribe query ${queryId} doesn't appear to start with 'subscription'`);
    }

    // const payload = {query: graph.buildQuery(query)} // we used to build queries with fragments
    const payload: SubscribePayload = { query };
    console.debug(`${queryId} (${role}): ${query}`);
    // Extra supported parameters
    if (extensions) payload.extensions = extensions;
    if (variables) {
      console.debug(`${queryId} VARS`, variables);
      payload.variables = variables;
    }
    if (operationName) payload.operationName = operationName;
    return this.getSocket(role).subscribe(payload, sink);
  }
}

// export an initialized instance
export const Gql = new _GqlInterface();
