import { derived, get, type Readable, writable, type Writable } from 'svelte/store';
import type { OidcManager } from '@auth/keycloak';
import type { EventConfiguration } from '@models/EventConfigurations';
import type { Team } from '@models/Team';
import Env from '@env';
import { EventRole } from '@auth/models';

export type Roles = {
  eventAdmin: boolean;
  ctfAdmin: boolean;
  participant: boolean;
  attacker: boolean;
  staff: boolean;
};

export type ScoreboardSettings = {
  flags: {
    ctf: boolean;
    infrastructure: boolean;
    inject: boolean;
    scoreChart: boolean;
    leaderboard: boolean;
    userDashboard: boolean;
  };
  ctf: {
    difficultyThresholds: {
      easy: number;
      medium: number;
      hard: number;
    };
  };
};

// Export a Svelte store for other elements to subscribe to
// DEPRECATED: Future use should reference oidc singleton directly
export const keycloak: Writable<OidcManager> = writable();
// browser ? new OidcManager(oidcSettings) : null,
export const team: Writable<{ id: string; name: string; long_name: string | null } | undefined> = writable();
export const eventCfg: Writable<EventConfiguration> = writable();
export const eventId: Writable<string> = writable(Env.EventID);

export const roles: Readable<Roles> = derived(keycloak, ($kc) => {
  return {
    eventAdmin: $kc.isRole(EventRole.EventAdmin),
    ctfAdmin: $kc.isRole(EventRole.CTFAdmin),
    participant: $kc.isRole(EventRole.Participant),
    attacker: $kc.isRole(EventRole.Attacker),
    staff: $kc.isRole(EventRole.Staff),
  };
});

export const settings: Writable<ScoreboardSettings> = writable({
  flags: {
    ctf: import.meta.env.VITE_FLAGS_CTF !== 'false',
    infrastructure: import.meta.env.VITE_FLAGS_INFRASTRUCTURE !== 'false',
    inject: import.meta.env.VITE_FLAGS_INJECT !== 'false',
    scoreChart: import.meta.env.VITE_FLAGS_SCORECHART !== 'false',
    leaderboard: import.meta.env.VITE_FLAGS_LEADERBOARD !== 'false',
    userDashboard: import.meta.env.VITE_FLAGS_USER_DASHBOARD !== 'false',
  },
  ctf: {
    difficultyThresholds: {
      easy: 0,
      medium: 50,
      hard: 150,
    },
  },
});
