export function isEmpty(obj: unknown): boolean {
  // if js thinks it's falsey, so do we
  if (!obj) return true;
  // false arrays don't count
  if (Array.isArray(obj) && obj.length === 0) return true;
  if (Object.keys(obj).length === 0 && Object.getPrototypeOf(obj) === Object.prototype) return true;
  return !obj;
}
