/**
 * Async sleep implementation
 * @param delay Delay in milliseconds
 */
export async function sleep(delay: number) {
  return new Promise((resolve) => setTimeout(resolve, delay));
}

/**
 * Poll a callback function until a condition is true
 * @param fetchCb
 * @param resultCb
 * @param delay
 */
export async function pollUntil<T = unknown>(fetchCb: () => Promise<T>, resultCb: (data: T) => boolean, delay = 3000) {
  let data = await fetchCb();
  while (!resultCb(data)) {
    await sleep(delay);
    data = await fetchCb();
  }
  return data;
}
