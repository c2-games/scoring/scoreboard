import type { AxiosResponse, Method } from 'axios';
import axios from 'axios';
import ENV from '../env';
import type { OidcManager } from '@auth/keycloak';
import { keycloak as keycloakStore } from '@stores';
import Env from '../env';
import type { Bundle, Environment, PveConfig, Template } from '@models/Sandbox';
import type { BundleCategory } from '@models/Sandbox';

type SandboxApiHeaders = {
  Authorization?: string;
  'Content-Type'?: string;
  'X-API-KEY'?: string;
};

let keycloak: OidcManager | undefined;
keycloakStore.subscribe((value) => (keycloak = value));

const axiosAPI = axios.create({
  baseURL: ENV.StorageApi,
});

function getHeaders(): SandboxApiHeaders {
  return {
    Authorization: `Bearer ${keycloak?.token}`,
    // 'X-API-KEY': 'dev key here',
  };
}

type SandboxParams = {
  data?: string | FormData | Record<string, unknown>;
  params?: Record<string, string | number | boolean>;
};

export async function sandboxApiRequest<T>(
  method: Method,
  path: string,
  { data, params }: SandboxParams = {},
): Promise<AxiosResponse<T>> {
  const headers = getHeaders();
  return axiosAPI({ method, url: `${Env.SandboxApiUri}${path}`, data, headers, params });
}

export type BundleListParams = {
  active?: boolean;
  category?: BundleCategory;
};

/*
 * Config
 */
export async function getPveConfig() {
  return (await sandboxApiRequest<PveConfig>('GET', '/config/proxmox')).data;
}

/*
 * Bundles
 */
export async function getBundles(params?: BundleListParams) {
  return (await sandboxApiRequest<Bundle[]>('GET', '/bundles', { params })).data;
}

export async function createBundle(bundle: Partial<Bundle>) {
  return (await sandboxApiRequest<Bundle>('POST', '/bundles', { data: bundle })).data;
}

export async function getBundleById(bundleId: string) {
  return (await sandboxApiRequest<Bundle>('GET', `/bundles/${bundleId}`)).data;
}

export async function deleteBundleById(bundleId: string) {
  return (await sandboxApiRequest<void>('DELETE', `/bundles/${bundleId}`)).data;
}

export async function updateBundleById(bundleId: string, bundle: Partial<Bundle>) {
  return (await sandboxApiRequest<Bundle>('PATCH', `/bundles/${bundleId}`, { data: bundle })).data;
}

/*
 * Templates
 */
export async function getTemplates() {
  return (await sandboxApiRequest<Template[]>('GET', '/templates')).data;
}

export async function createTemplate(template: Partial<Template>) {
  return (await sandboxApiRequest<Template>('POST', '/templates', { data: template })).data;
}

export async function getTemplateById(templateId: string) {
  return (await sandboxApiRequest<Template>('GET', `/templates/${templateId}`)).data;
}

export async function deleteTemplateById(templateId: string) {
  return (await sandboxApiRequest<void>('DELETE', `/templates/${templateId}`)).data;
}

export async function updateTemplateById(templateId: string, template: Partial<Template>) {
  return (await sandboxApiRequest<Template>('PATCH', `/templates/${templateId}`, { data: template })).data;
}

/*
 * Environments
 */
export type EnvironmentListParams = {
  // bundle_id?: string;  // new api
  bundle?: string;
  owner?: string;
};

export type NewEnvironment = {
  owner: string;
  // bundle_id: string; // new api
  bundle: string;
};

export type PVETask = {
  type: string;
  tokenid: string;
  pid: number;
  endtime: number;
  starttime: number;
  pstart: number;
  status: string;
  user: string;
  node: string;
  id: string;
  upid: string;
};

export async function getEnvironments(params: EnvironmentListParams) {
  return (await sandboxApiRequest<Environment[]>('GET', '/environments', { params })).data;
}

export async function createEnvironment(data: NewEnvironment) {
  return (await sandboxApiRequest<Environment>('POST', '/environments', { data })).data;
}

export async function getEnvironmentById(environmentId: string) {
  return (await sandboxApiRequest<Environment>('GET', `/environments/${environmentId}`)).data;
}

export async function getEnvironmentExists(environmentId: string): Promise<boolean> {
  let response;
  try {
    response = await sandboxApiRequest<Environment>('GET', `/environments/${environmentId}`);
  } catch (err) {
    // todo this request could fail for any reason, not just 404's
    return false;
    // console.log('catch err', err);
    // if ((err as AxiosError).code === '404') return false;
    // throw err;
  }
  return !!response.data;
}

/**
 * Get any active PVE tasks for an environment's VMs
 */
export async function getEnvironmentTasks(environmentId: string) {
  try {
    return (await sandboxApiRequest<PVETask[]>('GET', `/environments/${environmentId}/tasks`)).data;
  } catch (e) {
    // todo this catch handles a 404 case where the environment doesn't exist, but, this request could fail many ways
    return [];
  }
}

export async function destroyEnvironmentById(environmentId: string) {
  return (await sandboxApiRequest<Environment>('DELETE', `/environments/${environmentId}`)).data;
}

export async function startEnvironment(environmentId: string) {
  return (await sandboxApiRequest<Environment>('PUT', `/environments/${environmentId}/start`)).data;
}

export async function stopEnvironment(environmentId: string) {
  return (await sandboxApiRequest<Environment>('PUT', `/environments/${environmentId}/stop`)).data;
}
