import { marked } from 'marked';
import Code from '@components/Markdown/Code.svelte';
import CodeSpan from '@components/Markdown/CodeSpan.svelte';
import MarkdownContainer from '@components/Markdown/MarkdownContainer.svelte';
import Link from '@components/Markdown/Link.svelte';

// todo provide way to break out of a section - ex, `::` or `:::`
// todo support for textColor
const sectionExtension = {
  name: 'section',
  // Is this a block-level or inline-level tokenizer?
  level: 'block',
  // Hint to Marked.js to stop and check for a match
  start(src: string) {
    // return src.match(/:[^:\n]+:/);
    return src.match(/(?:\s|^):[^:\n]+:/)?.index;
  },
  tokenizer(src: string) {
    // Match any :text: pattern, capturing the label
    // simple match
    // const sectionHeaderRule = /^:([^:\n]+):/;
    // color match
    // const sectionHeaderRule =
    //   /^:([^:\n]+?)(?:\s+(?:(?:color=(#[0-9a-fA-F]{6}))|(?:bgColor=(#[0-9a-fA-F]{6})))){0,2}:/;
    const sectionHeaderRule =
      /(?:\s|^):([^:\n]+?)(?:\s+(?:(?:color=(#[0-9a-fA-F]{6}))|(?:bgColor=(#[0-9a-fA-F]{6})))){0,2}:/;

    const headerMatch = sectionHeaderRule.exec(src);

    if (!headerMatch) return;
    const fullHeader = headerMatch[0];
    const label = headerMatch[1];

    // Look for individual color matches
    const color = fullHeader.match(/color=(#[0-9a-fA-F]{6})/)?.[1];
    const bgColor = fullHeader.match(/bgColor=(#[0-9a-fA-F]{6})/)?.[1];

    // Find the start of the next section or end of text
    const remainingText = src.slice(headerMatch[0].length);
    const nextSectionIndex = remainingText.search(/(?:\s|^):[^:\n]+:/);
    let sectionContent = '';

    if (nextSectionIndex === -1) {
      // No next section found, consume all remaining text
      sectionContent = remainingText;
    } else {
      // Found next section, consume text up to that point
      sectionContent = remainingText.slice(0, nextSectionIndex);
    }

    // Create the token
    const token = {
      type: 'section',
      raw: fullHeader + sectionContent,
      label, // Store the text between colons as label
      color,
      bgColor,
      text: sectionContent, // Store the content after the label
      tokens: [],
    };

    // Parse the section content for inline and block tokens
    // @ts-expect-error lexer is defined by marked
    this.lexer.blockTokens(token.text, token.tokens);
    return token;
  },
};

const renderers = {
  code: Code,
  codespan: CodeSpan,
  section: MarkdownContainer,
  link: Link,
};

// @ts-expect-error the level is literally what you're asking for
marked.use({ extensions: [sectionExtension] });

export { marked, renderers };
