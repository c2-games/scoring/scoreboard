import { Log } from 'oidc-client-ts';

type Environment = {
  AssetStorage: string;
  GqlPublicUri: string;
  SandboxApiUri: string;
  TapiUri: string;
  OidcAuthority: string;
  OidcClientId: string;
  OidcLogLevel: Log;
  EventID: string;
  StorageApi: string;
  StorageBucket: string;
  StorageS3API: string;
  IntercomWarning?: string;
  IntercomError?: string;
};

const ENV: Environment = {
  AssetStorage: import.meta.env.VITE_ASSET_STORAGE_URI as string,
  GqlPublicUri: import.meta.env.VITE_GQL_PUBLIC_URI as string,
  TapiUri: import.meta.env.VITE_TAPI_URI as string,
  OidcAuthority: import.meta.env.VITE_OIDC_AUTHORITY as string,
  OidcClientId: import.meta.env.VITE_OIDC_CLIENT_ID as string,
  OidcLogLevel: import.meta.env.VITE_OIDC_DEBUG ? Log.DEBUG : Log.INFO,
  SandboxApiUri: import.meta.env.VITE_SANDBOX_API_URI as string,
  EventID: import.meta.env.VITE_EVENT_ID as string,
  StorageApi: import.meta.env.VITE_STORAGE_API as string,
  StorageBucket: import.meta.env.VITE_STORAGE_BUCKET as string,
  StorageS3API: import.meta.env.VITE_STORAGE_S3_API as string,
  IntercomWarning: (import.meta.env.VITE_INTERCOM_WARNING as string) || '',
  IntercomError: (import.meta.env.VITE_INTERCOM_ERROR as string) || '',
};

declare global {
  interface Window {
    C2GamesConfig: Environment;
  }
}

console.debug('ENV: ', ENV);
export default ENV;
