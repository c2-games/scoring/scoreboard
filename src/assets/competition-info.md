:**Discord** color=#9656ce:

> _**!IMPORTANT! All competition announcements and communication will be done through Discord**_
>
> Please ensure your Discord alias is set to your _**real name**_ so we know who you are

:**Infrastructure** color=#00FFFF:

##### Resources

- **Network Diagram**: [View Network Diagram](https://files.ncaecybergames.org/ncaecybergames/2025/regionals/NCAECyberGames-BlueTopo-2025.pdf)
  - There is also an interactive network diagram at the bottom of this page
- **Virtual Competition Platform**: [sandbox4.c2games.org](https://sandbox4.c2games.org:8006/#v1:0:=node%2Fsandbox4)
  - All VMs are here
- **Hackathon DNS**: 172.18.0.12
- **Hackathon ACME Server**: `https://ca.ncaecybergames.org`

  - This command may be useful 👀

  ```bash
  certbot --no-verify-ssl --server hackathon.ncaecybergames.org/acme/acme/directory
  ```

##### VM Console

To access your VM console:

1. Navigate to Proxmox: [sandbox4.c2games.org](https://sandbox4.c2games.org:8006/#v1:0:=node%2Fsandbox4)
   - ➔ Change Realm to: CNY Hackathon
   - _Team Credentials are pinned to your team's Discord channel_
2. Expand "sandbox4" on the left if you don't see your VMs

##### SPICE

Using the SPICE client in place of the standard VM Console will significantly improve your experience while dealing
directly with VMs that support it. SPICE allows for copy/paste functionality, as well as file transfer between your
local machine and the VM.
