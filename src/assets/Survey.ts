export enum SurveyQuestionType {
  Switch = 'Switch',
  Select = 'Select',
  Checkbox = 'Checkbox',
  ShortText = 'ShortText',
  LongText = 'LongText',
  SelectTeamMember = 'SelectTeamMember',
  Scale = 'Scale',
}

export type SurveyDependency = {
  prompt: string;
  values: (string | boolean)[];
};

export type SurveySection = {
  title: string;
  order: number;
  description?: string;
  questions: SurveyQuestion[];
  dependency?: SurveyDependency;
};

type BaseSurveyQuestion = {
  required: boolean;
  order: number;
  // groupId: string; // for database
  type: SurveyQuestionType | keyof typeof SurveyQuestionType;
  prompt: string;
  answer?: string;
  meta?: {
    // should this question be hidden until another question is answered a certain way?
    dependency?: SurveyDependency;
  };
};

export type SelectQuestion = BaseSurveyQuestion & {
  meta: {
    options: string[];
  };
};

export type CheckboxQuestion = BaseSurveyQuestion & {
  meta: {
    maxSelections?: number;
    minSelections?: number;

    options: string[];
  };
};

export type ScaleQuestion = BaseSurveyQuestion & {
  meta: {
    min: number;
    max: number;
    minLabel?: string;
    maxLabel?: string;
    maxSuffix?: string;
  };
};

export type SurveyQuestion = BaseSurveyQuestion | SelectQuestion | CheckboxQuestion | ScaleQuestion;

export type Survey = Record<string, SurveyQuestion>;

export let SurveyQuestions: SurveySection[] = [
  {
    title: 'NCAE Cyber Games Competition Survey',
    order: 10,
    description: 'Please provide feedback on your experience in the NCAE Cyber Games competition.',
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'Have you ever participated in the NCAE CyberGames previously?',
        type: 'Switch',
        // type: 'Select',
        // meta: {
        //   options: [
        //     'No, this is my first time',
        //     'Yes, this is my second year',
        //     'Yes, I have participated in two or more competition seasons',
        //   ],
        // },
      },
      {
        order: 30,
        required: true,
        prompt: 'How many years of experience do you have working in cybersecurity or a cybersecurity-related field?',
        type: 'Scale',
        meta: {
          min: 0,
          max: 4,
          maxSuffix: '+',
        },
        // type: 'Select',
        // meta: {
        //   options: ['0', '1', '2', '3', '4+'],
        // },
      },
      {
        order: 40,
        required: true,
        prompt: 'What was a challenge that you or your team overcame today?',
        type: 'LongText',
      },
      {
        order: 50,
        required: true,
        prompt: 'Were there any funny or interesting moments you or your team encountered today?',
        type: 'LongText',
      },
      {
        order: 60,
        required: true,
        prompt: 'Did you learn anything new today?',
        type: 'LongText',
      },
    ],
  },
  {
    title: 'Survey Awards',
    order: 20,
    description: 'Nominate your teammates for the following awards, they could win awesome prizes!',
    questions: [
      {
        order: 10,
        required: true,
        prompt:
          'Vote: Most Valuable Player - Who helped out the team the most? This could be someone who scored the most points for the team, someone who helped out their teammates the most, or someone who kept everyone organized and focused.',
        type: 'SelectTeamMember',
      },
      {
        order: 20,
        required: true,
        prompt:
          'Vote: Most Improved - The NCAE Cyber Games are all about learning and improvement! Who on your team improved the most during this competition?',
        type: 'SelectTeamMember',
      },
      {
        order: 30,
        required: true,
        prompt:
          'Vote: Speaker Closing Ceremony - Please nominate a member of your team to speak at the closing ceremony.',
        type: 'SelectTeamMember',
      },
    ],
  },
  {
    title: 'Prizes',
    order: 22,
    description: 'Prizes will be shipped to your institution as a package.',
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'Unisex T-Shirt Size',
        type: 'Select',
        meta: {
          options: ['Small', 'Medium', 'Large', 'XL', '2XL', '3XL', "I don't want a shirt"],
        },
      },
    ],
  },
  {
    title: 'Event Registration',
    order: 25,
    description: 'How was your experience registering for the event?',
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'How did you first learn about the competition?',
        type: 'Checkbox',
        meta: {
          options: [
            'Competition flyer',
            'Social Media/Online',
            'Professor/Faculty',
            'Student Colleague',
            'Campus/University Club',
            'Other',
          ],
        },
      },
      {
        order: 11,
        required: true,
        prompt: 'Please specify how you first learned about the competition:',
        type: 'ShortText',
        meta: {
          dependency: {
            prompt: 'How did you first learn about the competition?',
            values: ['Other'],
          },
        },
      },
      {
        order: 20,
        required: true,
        prompt: 'I found the process to enroll in the competition intuitive and easy',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
    ],
  },
  {
    title: 'NCAE Sandbox',
    order: 30,
    description: 'How useful was the NCAE Sandbox in preparing for this event?',
    questions: [
      {
        required: true,
        order: 10,
        prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
        type: 'Switch',
        // type: 'Select',
        // meta: {
        //   options: ['Yes', 'No', "No, I wasn't aware of the sandbox challenges"],
        // },
      },
      {
        required: true,
        order: 15,
        prompt: 'Were you aware that the sandbox challenges existed?',
        type: 'Switch',
        meta: {
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: [false],
          },
        },
      },
      {
        order: 20,
        required: true,
        prompt: 'The sandbox challenges were useful in preparing for the competition ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 30,
        required: true,
        prompt: 'The sandbox challenges assisted to improve personal critical thinking and analytical skills ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 40,
        required: true,
        prompt: 'The sandbox challenge environment was intuitive / easy to navigate and utilize ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 50,
        required: true,
        prompt: 'The sandbox challenges represented real world cyber scenarios ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 55,
        required: true,
        prompt: 'I was able to access the sandbox and tutorials as often and when convenient for my schedule ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 60,
        required: true,
        prompt: 'Our school / club utilized the sandbox challenges as part of the curriculum / club activities ',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the sandbox challenges?',
            values: ['Yes', true],
          },
        },
      },
    ],
  },
  {
    title: 'NCAE Tutorials',
    order: 40,
    description: 'How useful were the NCAE Tutorial Videos in preparing for this event?',
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'In preparation for the competition, did you utilize the tutorials?',
        type: 'Switch',
        // type: 'Select',
        // meta: {
        //   options: ['Yes', 'No', "No, I wasn't aware of the tutorials"],
        // },
      },
      {
        required: true,
        order: 15,
        prompt: 'Were you aware that the NCAE tutorials existed?',
        type: 'Switch',
        meta: {
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the tutorials?',
            values: [false],
          },
        },
      },
      {
        order: 20,
        required: true,
        prompt: 'The tutorials were useful in preparing for the competition',
        type: 'Scale',
        meta: {
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the tutorials?',
            values: ['Yes', true],
          },
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 30,
        required: true,
        prompt: 'The tutorials assisted to improve personal critical thinking and analytical skills',
        type: 'Scale',
        meta: {
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the tutorials?',
            values: ['Yes', true],
          },
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 40,
        required: true,
        prompt: 'Our school / club utilized the tutorials as part of our curriculum / club activities',
        type: 'Scale',
        meta: {
          dependency: {
            prompt: 'In preparation for the competition, did you utilize the tutorials?',
            values: ['Yes', true],
          },
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
    ],
  },
  {
    title: 'Capture the Flag',
    order: 50,
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'During the competition, did you interact with the CTF?',
        type: 'Switch',
      },
      {
        order: 20,
        required: true,
        meta: {
          dependency: { prompt: 'During the competition, did you interact with the CTF?', values: ['Yes', true] },
        },
        prompt: 'Have you ever participated in a CTF-style competition prior to this event?',
        type: 'Switch',
      },
      {
        order: 30,
        required: true,
        prompt: 'The CTF was well suited for a participant with entry level / beginner skills',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: { prompt: 'During the competition, did you interact with the CTF?', values: ['Yes', true] },
        },
      },
      {
        order: 40,
        required: true,
        prompt: 'The CTF presented a broad range of both technical and non-technical challenges',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: { prompt: 'During the competition, did you interact with the CTF?', values: ['Yes', true] },
        },
      },
      {
        order: 50,
        required: true,
        prompt:
          'After being involved in the NCAE Cyber Games CTF, I am interested in participating in more advanced CTF-style competitions',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: { prompt: 'During the competition, did you interact with the CTF?', values: ['Yes', true] },
        },
      },
    ],
  },
  {
    title: 'Services and Infrastructure',
    order: 60,
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'During the competition, did you interact with the service/infrastructure component?',
        type: 'Switch',
      },
      {
        order: 20,
        required: true,
        meta: {
          dependency: {
            prompt: 'During the competition, did you interact with the service/infrastructure component?',
            values: ['Yes', true],
          },
        },
        prompt: 'Have you participated in a service-based infrastructure competition like this before?',
        type: 'Switch',
      },
      {
        order: 30,
        required: true,
        prompt:
          'The service/infrastructure challenge included an appropriate number of challenges for a beginner skill level',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'During the competition, did you interact with the service/infrastructure component?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 40,
        required: true,
        prompt: 'The service/infrastructure component included an appropriate amount of difficult challenges',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'During the competition, did you interact with the service/infrastructure component?',
            values: ['Yes', true],
          },
        },
      },
      {
        order: 50,
        required: true,
        prompt: 'The competition presented a broad range of both technical and non-technical challenges',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
          dependency: {
            prompt: 'During the competition, did you interact with the service/infrastructure component?',
            values: ['Yes', true],
          },
        },
      },
    ],
  },
  {
    title: 'Overall Competition Feedback',
    order: 70,
    questions: [
      {
        order: 10,
        required: true,
        prompt:
          'After being involved in the NCAE Cyber Games, I am interested in participating in more advanced cyber competitions',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 20,
        required: true,
        prompt: 'The roles of the support teams (Blue, Orange, Black, and Red Teams) were well understood',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 40,
        required: true,
        prompt: 'The competition was fun',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 50,
        required: false,
        prompt:
          'What were your personal goals for the competition?  If none, please leave blank and proceed to the next question.',
        type: 'LongText',
      },
      {
        order: 60,
        required: false,
        prompt:
          'As it pertains to the Competition, please list any additional comments or feedback (specifically what worked well, what did not work well, and what can be improved for future competitions).  If none, please leave blank and proceed to the next question.',
        type: 'LongText',
      },
    ],
  },
  {
    title: 'Discord',
    order: 80,
    questions: [
      {
        order: 10,
        required: true,
        prompt: 'Discord helped to collaborate within our team',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 20,
        required: true,
        prompt: 'Discord helped to maintain awareness during the competition',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
      {
        order: 30,
        required: true,
        prompt: 'Discord assisted in communicating with the support teams',
        type: 'Scale',
        meta: {
          min: 1,
          max: 5,
          minLabel: 'Strongly Disagree',
          maxLabel: 'Strongly Agree',
        },
      },
    ],
  },
];
