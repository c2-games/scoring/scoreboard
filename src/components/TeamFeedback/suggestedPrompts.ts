export type FeedbackProvider = keyof typeof suggestedPrompts;
export type FeedbackPrompt = {
  prompt: string;
  answer?: string;
};

export const suggestedPrompts: Record<string, FeedbackPrompt[]> = {
  'Black Team': [
    { prompt: 'How well did your team work together?' },
    { prompt: 'What skills should this team work on?' },
    { prompt: 'Any additional feedback?' },
  ],
  'Red Team': [{ prompt: 'How well did your team defend services?' }, { prompt: 'Any additional feedback?' }],
  'Orange Team': [
    { prompt: 'How well did your team solve CTFs?' },
    { prompt: 'Any outside-the-box strategies?' },
    { prompt: 'Any additional feedback?' },
  ],
};
