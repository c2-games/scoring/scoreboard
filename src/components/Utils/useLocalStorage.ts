import { writable } from 'svelte/store';
import type { Writable } from 'svelte/store';

type ValueType = string | number | boolean | object | undefined | any[];

export function useLocalStorage<T extends ValueType>(key: string, initialValue: T): Writable<T> {
  let storeValue: T;

  try {
    const currentValue: string | null = localStorage.getItem(key);
    storeValue = currentValue ? JSON.parse(currentValue) : initialValue;
  } catch (e) {
    console.error(`Error retrieving ${key} from localStorage`, e);
    console.debug('clearing local storage for', key);
    // clear invalid entry
    localStorage.removeItem(key);
    storeValue = initialValue;
  }

  let store: Writable<T> = writable(storeValue);

  store.subscribe((value) => {
    // don't store undefined
    if (value === undefined) {
      console.debug('clearing local storage for', key);
      localStorage.removeItem(key);
      return;
    }

    try {
      localStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      console.error(`Error storing ${key} to localStorage`, e);
    }
  });

  return store;
}
