import { writable } from 'svelte/store';
import type { ScoreChartScores } from '@models/ScoreChart';

export type BonusCategorySummary = {
  [category: string]: number;
};

export const ScoresDataStore = writable<ScoreChartScores | null>(null);
