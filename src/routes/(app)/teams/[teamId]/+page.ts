import type { AfterLoadEvent } from './$houdini';

export const ssr = false;
export const _houdini_afterLoad: AfterLoadEvent = ({ event }) => {
  return {
    teamId: event.params.teamId,
  };
};
