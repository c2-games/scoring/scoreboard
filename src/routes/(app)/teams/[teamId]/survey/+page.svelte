<script lang="ts">
  import type { PageData } from "./$types";
  import { Button, Form, Spinner } from "@sveltestrap/sveltestrap";
  import { eventId, keycloak, team as userTeam } from "@stores";
  import {
    SubmitWrapupFeedbackStore,
    SurveyFeedbackStore,
    type wrapup_feedback_insert_input
  } from "$houdini";
  import SurveyQuestion from "@components/Survey/SurveyQuestion.svelte";
  import {
    type SurveyQuestion as SurveyQuestionT,
    type Survey,
    SurveyQuestions
  } from "@assets/Survey";
  import { getNotificationsContext } from "svelte-notifications";
  import { requireTeam } from "@auth/PermissionRequired";
  import { goto } from "$app/navigation";
  import Fa from "svelte-fa";
  import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";

  const { addNotification } = getNotificationsContext();

  export let data: PageData;
  $: teamId = data.teamId;

  const role = "participant"; // query role
  let submitting = false;
  let previouslySubmittedCount = 0;
  const previousSubmissions = new SurveyFeedbackStore()
  let missing: SurveyQuestionT[] = [];
  // map of prompt: question
  let questions = SurveyQuestions.flatMap((s) => s.questions);
  let survey: Survey = Object.fromEntries(questions.map((q) => [q.prompt, q]));

  async function ready() {
    // short circuit for staff. we don't want to load previous results or redirect
    if($keycloak.isStaff()) return;

    await requireTeam();

    // redirect user to the correct team survey
    if (teamId !== $userTeam?.id) {
      await goto("/teams/" + $userTeam?.id + "/survey");
    }

    // get the number of previous submissions only after we've verified
    // the user is logged in viewing the correct team
    void getPreviousSubmissions();
  }

  async function getPreviousSubmissions() {
    const response = await previousSubmissions.fetch({
      metadata: { role },
      variables: {
        where: {
          team_id: { _eq: teamId },
          event_id: { _eq: $eventId },
          author_id: { _eq: $keycloak.userId }
        }
      }
    });
    if (response.errors) {
      console.error(response.errors);
      return;
    }

    previouslySubmittedCount = response.data?.wrapup_feedback_aggregate.aggregate?.count || 0;
  }

  if (Object.keys(survey).length !== questions.length) {
    throw new Error("Duplicate prompts found in SurveyQuestions!");
  }

  function shouldDisplay(survey: Survey, question: SurveyQuestionT): boolean {
    // no dependencies, display all
    if (!question.meta?.dependency) {
      return true;
    }

    let { prompt, values } = question.meta.dependency;
    // let dependency = survey.find((q) => q.prompt === prompt);
    let dependency = survey[prompt];

    // handle unknown dependency
    if (!dependency || dependency.answer === undefined) {
      console.warn(`Dependency not found! Question=${question.prompt} Dependency=${prompt}`);
      // default to hiding the question if dependency is not found
      // or it's not yet answered
      return false;
    }

    if (["Checkbox", "Scale", "ShortText", "LongText"].includes(dependency.type)) {
      // todo not sure if this handles numbers from 'scale' or bool from 'switch'
      return values.some((v: string | boolean) => dependency.answer?.includes(v));
    }

    return values.includes(dependency.answer);
  }

  function validate(questions: SurveyQuestionT[]): boolean {
    missing = [];
    for (let question of questions) {
      let required = question.required || false;

      if (!required || question.answer) {
        // question isn't required, or it's been answered, so short circuit.
        continue;
      }

      if (question.meta?.dependency) {
        // if the question has a dependency, this question is only required
        // if the dependency is also satisfied. Aka, allow setting required=true,
        // but don't enforce it if the question is not displayed.
        required = required && shouldDisplay(survey, question);
      }

      if (required && question.answer === undefined) {
        console.warn(`MISSING dep=${!!question.meta?.dependency} required=${question.required} isRequired=${required}`);
        missing.push(question);
      }
    }

    console.debug("missing", missing);

    if (missing.length > 5) {
      addNotification({
        position: "bottom-right",
        text: `${missing.length} required items still need a response`,
        type: "error",
        removeAfter: 5000
      });
      return false;
    } else if (missing.length) {
      missing.forEach((q) => {
        addNotification({
          position: "bottom-right",
          text: `Required item needs a response "${q.prompt}"`,
          type: "error",
          removeAfter: 5000
        });
      });
      return false;
    }

    return true;
  }

  async function submit(questions: SurveyQuestionT[]) {
    if (!validate(questions)) {
      throw new Error("Survey is not valid!");
    }

    let submissions: wrapup_feedback_insert_input[] = [];
    for (let q of questions) {
      if (q.answer !== undefined) {
        submissions.push({
          source: "participant",
          group: "Competition Survey",
          prompt: q.prompt,
          answer: JSON.stringify(q.answer),
          meta: q.meta || null,
          team_id: teamId,
          event_id: $eventId
        });
      }
    }

    console.debug(submissions);

    const result = await new SubmitWrapupFeedbackStore().mutate(
      { input: submissions },
      { metadata: { role } }
    );

    if (result.errors) {
      console.error(result.errors);
      result.errors.forEach((e: unknown) => addNotification({
        text: (e as Error).message,
        position: "bottom-right",
        type: "error"
      }));
      throw new Error("Failed to submit Survey");
    } else {
      addNotification({
        position: "bottom-right",
        text: "Response saved!",
        type: "success",
        removeAfter: 3000
      });
    }
  }

  // wrapper to submit survey
  async function onSubmit() {
    submitting = true;
    try {
      await submit(Object.values(survey));
    } catch (e) {
      console.error(e);
      addNotification({
        // todo better error message
        text: `Failed to submit Survey`,
        position: "bottom-right",
        type: "error",
        removeAfter: 4000
      });
      return;
    } finally {
      submitting = false;
    }

    await goto('/teams/' + teamId);
  }

  function onChange(evt: CustomEvent<{ question: SurveyQuestionT, answer: string }>) {
    const question = evt.detail.question;
    const answer = JSON.parse(evt.detail.answer);
    survey = {
      ...survey,

      [question.prompt]: {
        ...question,
        answer
      }
    };

    // remove from missing once it's been answered
    console.log("onChange", question, answer, missing, missing.filter(q => q.prompt !== question.prompt));
    if (answer !== undefined) missing = missing.filter(q => q.prompt !== question.prompt);
  }

  function clearSurvey() {
    if (confirm("ARE YOU SURE? This will remove ALL answers from the survey!")) {
      for (let q of SurveyQuestions.flatMap((s) => s.questions)) {
        localStorage.removeItem(q.prompt);
      }

      location.reload();
    }
  }

  function loadSurvey() {
    if(!$previousSubmissions.data?.wrapup_feedback?.length) {
      alert('no previous submissions found');
      return
    }

    if (confirm("ARE YOU SURE? This will replace ALL answers in the survey form with those from the database!")) {
      for (let q of SurveyQuestions.flatMap((s) => s.questions)) {
        localStorage.removeItem(q.prompt);
      }

      for(let q of $previousSubmissions.data.wrapup_feedback) {
        localStorage.setItem(q.prompt, q.answer);
      }

      location.reload();
    }
  }
</script>

<svelte:head>
  <title>Survey</title>
</svelte:head>

<div class="d-flex flex-column flex-md-row gap-3 sticky-top bg-body p-3">
  <h3 class="flex-grow-1">NCAE Cyber Games Competition Survey</h3>
  <span class="mx-auto flex-grow-1 text-warning">
    {#if previouslySubmittedCount}
      <Fa icon={faExclamationCircle} />
      {previouslySubmittedCount} answers previously submitted.
      <br />
      Your answers may be updated by pressing submit.
    {/if}
  </span>
  <Button color="primary" on:click={loadSurvey}>Load Survey</Button>
  <Button color="danger" on:click={clearSurvey}>Clear Survey</Button>
  <Button disabled={submitting} on:click={onSubmit} color="success">
    Submit
    {#if submitting}
      <Spinner />
    {/if}
  </Button>
</div>

{#await ready() then _}
  <div class="container">
    <Form class="bg-dark rounded p-4 mx-auto" style="max-width: 800px;">
      {#each SurveyQuestions as section}
        <h2>{section.title}</h2>
        {#if section.description}
          <p>{section.description}</p>
        {/if}

        {#each section.questions as question}
          {#if shouldDisplay(survey, question)}
            <div class="py-3">
              {#if missing.find((q) => q.prompt === question.prompt)}
                <p class="text-danger">This question is required</p>
              {/if}
              <SurveyQuestion on:change={onChange} {teamId} entry={question} />
            </div>
          {/if}
        {/each}
      {/each}
    </Form>
  </div>
{/await}
