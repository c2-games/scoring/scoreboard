// disable SSR because we use +page.gql files, which require a user token that we do not have during SSR
export const ssr = false;

export const load = ({ params }) => {
  return {
    teamId: params.teamId,
  };
};
