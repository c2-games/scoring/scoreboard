export function getVariablesByPk(type: string, id: string) {
  return `{
    variables_${type}_by_pk(${type}_id: ${JSON.stringify(id)}) {
        ${type}_id
        vars
      }
    }`;
}

export function getVariablesList(type: string) {
  const idField = type === 'deployment' ? 'deploy' : type;
  return `{
    variables_${type} {
        ${idField}_id
        vars
        ${idField} {
          name
        }
      }
    }`;
}
