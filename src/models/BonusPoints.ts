import type { Team } from './Team';
import type { EventConfiguration } from '@models/EventConfigurations';

export type BonusPoints = {
  id: string;
  team_id: string;
  team: Team;
  event_id: string;
  event: EventConfiguration;
  points: number;
  category: string;
  reason: string;
  time?: Date;
};

export type BonusPointsReturn = {
  scoring_bonus_points: Array<BonusPoints>;
};

export function BonusPointsEventQuery(eventId: string) {
  return `{
    scoring_bonus_points(
      where: {event_id: {_eq: ${JSON.stringify(eventId)}}},
      , limit: 100, order_by: {time: desc}
    ) {
      event_id
      event {
        id
        name
        start
        end
      }
      team_id
      team {
        id
        name
      }
      category
      points
      reason
      time
    }
  }`;
}

export const BonusPointCategoriesQuery = `{
  scoring_bonus_points(distinct_on: category) {
    category
  }
}`;
