//TODO: Deprecate all this code
export type ScoringBreak = {
  start: string;
  end: string;
};

export type EventConfiguration = {
  id: string;
  name: string;
  start: string | null;
  started: boolean | null;
  end: string | null;
};

export type EventConfigurationReturn = {
  events: Array<EventConfiguration>;
};

export function EventCfgByIdQuery(eventId: string): string {
  return `{
    events_by_pk(id: ${JSON.stringify(eventId)}) {
      id
      name
      start
      started
      end
      breaks(order_by: {start: asc, end: asc}) {
        start
        end
      }
    }
  }`;
}
export const AllEventCfgQuery = `{
  events(order_by: {start: asc}) {
    id
    name
    start
    end
    breaks(order_by: {start: asc, end: asc}) {
      start
      end
    }
  }
}`;
