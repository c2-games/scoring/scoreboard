import { Gql } from '../db';
import env from '@env';

export type Environment = {
  id?: string;
  event_id?: string;
  name?: string;
  info?: string;
  scoring_details?: string;
  topology?: string;
};

export type Event = {
  id?: string;
  name?: string;
  start?: string;
  end?: string;
  environments?: Array<Environment>;
};
export type EventSettings = {
  event?: Event;
  event_id?: string;
  org_name?: string;
  org_link?: string;
  info_link?: string;
  scoring_details?: string;
  name: string;
};

export type EventSettingsReturn = {
  scoreboard_event_settings: Array<EventSettings>;
};

const updateEnvQuery = `mutation updateEnvironment($name: String!, $input: environments_set_input!) {
  update_environments(where:{name: {_eq:$name}}, _set: $input) {
    affected_rows
  }
}`;

export async function updateEnvironment(environment: Environment) {
  const response = await Gql.query<{ update_environments: { affected_rows: number } }>(updateEnvQuery, {
    variables: {
      name: environment.name,
      input: environment,
    },
  });
  return response.update_environments.affected_rows;
}

export const AllEventSettingsQuery = `{
    scoreboard_event_settings(order_by: {event_id: asc}) {
      event {
        id
        name
        start
        end
        environments {
          info
        }
      }
      info_link
      org_link
      org_name
    }
  }`;

export function EventSettingsQuery(eventId: string) {
  return `{
      scoreboard_event_settings(
        where: {
          event_id: {_eq: ${JSON.stringify(eventId)}}
        }
      ) {
        event {
          id
          name
          start
          end
          results_published
          environments {
            name
            info
            scoring_details
            topology
          }
        }
        info_link
        org_link
        org_name
      }
    }`;
}

export async function GetEventSettings(eventId: string): Promise<EventSettings> {
  const response = await Gql.query<EventSettingsReturn>(`query ${EventSettingsQuery(eventId)}`);
  const values = response.scoreboard_event_settings;
  if (values.length < 1) {
    throw `no event found with id '${eventId}'`;
  }
  return values[0];
}

export function GetEnvironmentTopology(settings?: EventSettings) {
  if (settings?.event?.environments?.length && settings.event.environments[0].topology) {
    let topology = settings.event.environments[0].topology;
    if (topology.startsWith('json:')) {
      return JSON.parse(topology.split('json:', 2)[1]);
    } else {
      console.log('Unsupported topology defined');
      return null;
    }
  }
  return null;
}
