import type { Team } from './Team';
import type { BonusCategorySummary } from '@components/ScoreChart/scoresDataStore';

export type Scores = {
  ctf: number;
  bonus: number;
  service: number;
  total: number;
  team: Partial<Team>;
  bonus_by_category: Array<{ category: string; score: number }>;
};

export type ScoreChartScores = {
  name?: string;
  long_name?: string;
  ctf: number;
  service: number;
  total: number;
  bonus_by_category: Array<{ category: string; score: number }>;
  team: Partial<Team>;
  bonus_by_category_summary: BonusCategorySummary;
};

export type ScoreResponse = {
  scores: Array<Scores>;
};

export function ScoresQuery(eventId: string) {
  return `{
    scoreboard_team_summary(
      order_by: {order: asc},
      where: {event_id: {_eq: ${JSON.stringify(eventId)}}}
    ) {
      score {
        bonus
        ctf
        event_id
        service
        team_id
        total
        team {
          id
          name
          long_name
        }
        bonus_by_category {
          category
          score
        }
      }
    }
  }`;
}
