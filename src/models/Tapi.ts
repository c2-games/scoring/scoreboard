export type TapiUser = {
  id: number;
  email: string;
  keycloakSid: string; // uuid
  enabled: boolean;
  createdByUserId: number;
  createdDate: string; //date
  updatedByUserId: number;
  updatedDate: string; // date
};

export type TapiEventTeamUser = {
  id: number;
  enabled: boolean;
  createdByUserId: number;
  createdDate: string; //date
  updatedByUserId: number;
  updatedDate: string; // date

  eventTeamId: number;
  userId: number;
  addedOn: string; // date
  user: TapiUser;
};

export type TapiEventTeam = {
  id: number;
  enabled: boolean;
  createdByUserId: number;
  createdDate: string; //date
  updatedByUserId: number;
  updatedDate: string; // date

  eventId: number;
  name: string;
  eventTeamUsers: TapiEventTeamUser[];
};

export type TapiEventExport = {
  id: number;
  enabled: boolean;
  createdByUserId: number;
  createdDate: string; //date
  updatedByUserId: number;
  updatedDate: string; // date

  eventId: number;
  name: string;
  eventTeamUsers: TapiEventTeamUser[];

  regions: string[];
  startsOn: string; // date
  rules: string;
  eventTeams: TapiEventTeam[];
};
