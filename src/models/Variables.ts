import type { Team } from '@models/Team';
import type { Check } from '@models/Service';
import type { EventConfiguration } from '@models/EventConfigurations';

export type TeamVariables = {
  team_id: string; // UUID
  vars: Record<string, unknown> & {
    id: number;
  };
  team?: Team;
};

export type DeploymentVariables = {
  deploy_id: string; // UUID
  vars: Record<string, unknown>;
  deployed?: {
    check: Check;
    event: EventConfiguration;
  };
};

export type CheckVariables = {
  check_id: string; // UUID
  vars: Record<string, unknown>;
  check?: Check;
};

export type EventVariables = {
  event_id: string; // UUID
  vars: Record<string, unknown> & {
    checks: {
      host: string;
      name: string;
      subnet: string;
    };
    subnets: {
      internal: string;
      external: string;
    };
  };
  event?: EventConfiguration;
};
