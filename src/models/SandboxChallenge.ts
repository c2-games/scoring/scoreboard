export type SandboxChallengeTemplate = {
  template_id: string;
  user_role: 'NoAccess' | 'PVEVMUser';
};

export type SandboxChallenge = {
  id: string;
  name: string;
  description: string;
  templates: SandboxChallengeTemplate[];
};
