import { Gql } from '@db';
import type { NullableEventRole } from '@auth/models';

export type TeamUser = {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
};

export type Team = {
  id?: string;
  name: string;
  long_name?: string;
  tapi_id: number;
  competition_id?: number;
};

export type TeamWithId = Team & { id: string };

export type TeamSettings = {
  visible: boolean;

  event_id: string;
  team_id: string;

  team: TeamWithId;
  event: Event;
};

export type TeamResponse = {
  teams: Array<Team>;
};

/**
 * Retrieve Team from Database
 * @param teamId UUID of Team to Retrieve
 * @param role Optionally provide a role to perform the query with. The Staff role will grant access to hidden teams.
 */
export async function TeamById(teamId: string, role?: NullableEventRole): Promise<Team> {
  const teamByIdQuery = `query team_by_id {
        teams(where: {id: {_eq: ${JSON.stringify(teamId)}}}) {
            id
            name
            long_name
            competition_id
        }
    }`;

  try {
    const values = (await Gql.query<TeamResponse>(teamByIdQuery, { role })).teams;
    if (values.length < 1) {
      throw `no team found with id '${teamId}'`;
    }
    return values[0];
  } catch (error) {
    const msg = `error retrieving team id '${teamId}'`;
    console.error(msg, error);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    throw new Error(msg, { cause: error });
  }
}

/**
 * Retrieve Team from Database
 * @param userId UUID of Team to Retrieve
 * @param eventId UUID of Event the Team should belong to
 * @param role Optionally provide a role to perform the query with. The Staff role will grant access to hidden teams.
 */
export async function UserTeamByEventId(eventId: string, userId: string, role?: NullableEventRole): Promise<Team> {
  const teamByIdQuery = `query UserTeamByEventId{
    teams(where: {
      members: {user_id: {_eq: ${JSON.stringify(userId)}}},
      events: {event_id: {_eq: ${JSON.stringify(eventId)}}}
    }) {
        id
        name
    }
  }`;

  try {
    const values = (await Gql.query<TeamResponse>(teamByIdQuery, { role })).teams;
    if (values.length < 1) {
      throw `no team found for user id '${userId}' participating on event id '${eventId}'`;
    }
    return values[0];
  } catch (error) {
    const msg = `no team found for user id '${userId}' participating on event id '${eventId}'`;
    throw new Error(msg);
  }
}

export async function getTeammates(teamId: string) {
  return (
    await Gql.query<{ team_membership: { user: TeamUser[] } }>(`query GetTeammates {
    team_membership(
      where: {
        team_id: {
          _eq: ${JSON.stringify(teamId)}
        }
      }
    ) {
      user {
        email
        first_name
        last_name
        id
      }
    }
  }`)
  ).team_membership.map((t) => t.user);
}

/**
 * Get All teams
 * @param eventId Event to query teams for
 * @param role Optionally provide a role to perform the query with. The Staff role will grant access to hidden teams.
 */
export async function GetAllTeams(eventId: string): Promise<TeamWithId[]> {
  const allTeams = `query GetTeams {
    scoreboard_team_summary(
      where: {
        event_id: { _eq: ${JSON.stringify(eventId)} },
      },
      order_by: {order: asc}
    ) {
      team {
        id
        name
        long_name
        tapi_id
      }
  }
}
`;
  const data = await Gql.query<{ scoreboard_team_summary: TeamSettings[] }>(allTeams);
  return data.scoreboard_team_summary.map((s) => s.team);
}
