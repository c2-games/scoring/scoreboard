import { Gql, type QueryOptions } from '@db';
import type { Team } from '@models/Team';

export type Service = {
  id: number;
  name: string;
  display_name: string;
  description: string;
};

export type ServiceStatus = {
  check_id: number;
  check_time: string;
  result_code: string;
  visible: boolean;
  event_id: string;
  participant: FeedbackDetails | null;
  check: Check;
};

export type DisplayedService = {
  check_id: string;
  event_id: string;
  order: number;

  check?: Partial<Check>;
};

export type Check = {
  display_name: string;
  description: string;
  id: string;
  name: string;
  details?: string;
};

export type ScoringPoints = {
  period_id: string;
  check_id: string;
  points: number;
  interval: number;
  check: Check;
};

export type ScoringPeriod = {
  id: string;
  event_id: string;
  start: string;
  end: string;
  points: Array<ScoringPoints>;
};

export type FeedbackDetails = {
  feedback: string;
  detailed_results: Array<unknown>;
};

export type CheckResultRawExecData = {
  exec: {
    args: string[];
    cmd: string;
    end_time: string;
    errors: string[];
    post_delay_time: string;
    start_time: string;
    stderr: string;
    stdout: string;
  };
};

export type ScoreBoardServiceStatus = {
  event_id: string;
  team: Team;
  check_time: string;
  result_code: string;
  visible: boolean;
  participant?: FeedbackDetails;
  staff?: FeedbackDetails;
  data?: CheckResultRawExecData;
  check: Check;
};

export function ServiceFeedbackQuery(event: { id: string }, team: { id: string }, staff: boolean, data: boolean) {
  let q = `ServiceStatus {
    scoreboard_service_status(
      order_by: {team_order: asc, service_order: asc},
      where: {
        event_id: {_eq: ${JSON.stringify(event.id)}}
        team: {id: {_eq: ${JSON.stringify(team.id)}}}
      }
    ) {
      event_id
      team {
        id
        name
      }
      check {
        id
        name
        display_name
        description
      }
      visible
      participant\n`;
  q += staff ? `staff\n` : '';
  q += data ? `data\n` : '';
  q += `result_code
      check_time
    }
  }`;
  return q;
}

export function ServiceStatusQuery(eventId: string) {
  return `ServiceStatus {
    scoreboard_service_status(
      order_by: {team_order: asc, service_order: asc},
      where: {event_id: {_eq: ${JSON.stringify(eventId)}}}
    ) {
      event_id
      team {
        id
        name
        long_name
      }
      check {
        id
        name
        display_name
        description
      }
      visible
      participant
      result_code
      check_time
    }
  }`;
}

export async function GetDisplayedServices(
  eventId: string,
  type = 'service',
  options?: QueryOptions,
): Promise<DisplayedService[]> {
  const q = `query ServiceStatus {
    scoreboard_displayed_services(
      order_by: {order: asc},
      where: {
        event_id: {_eq: ${JSON.stringify(eventId)}},
        check: {type: {_eq: ${JSON.stringify(type)}}}
      }
    ) {
      check {
        display_name
      }
      check_id
      event_id
      order
    }
  }`;
  return (await Gql.query<{ scoreboard_displayed_services: DisplayedService[] }>(q, options))
    .scoreboard_displayed_services;
}

export async function GetServiceNames(eventId: string) {
  let ret: Partial<Check>[] = [];
  const q = `query ServiceStatus {
    scoreboard_displayed_services(
      order_by: {order: asc},
      where: {
        event_id: {_eq: ${JSON.stringify(eventId)}},
        check: {type: {_eq: "service"}}
      }
    ) {
      check {
        display_name
      }
    }
  }`;
  await Gql.query(q)
    .then((res) => {
      ret = (
        res as { scoreboard_displayed_services: Array<{ check: Partial<Check> }> }
      ).scoreboard_displayed_services.map((e) => {
        return e.check;
      });
    })
    .catch((err) => {
      throw err;
    });
  return ret;
}

export async function GetScoringPeriodsPoints(eventId: string) {
  const q = `query ServicePointsTable {
    scoring_periods(where: {event_id: {_eq: ${JSON.stringify(eventId)}}}) {
      event_id
      start
      end
      points(where: {check:{type: {_eq: "service"}}}) {
        check {
          id
          name
          display_name
          description
          details
        }
        points
        interval
      }
    }
  }`;
  const response = await Gql.query<{ scoring_periods: Array<Partial<ScoringPeriod>> }>(q);
  const values = response.scoring_periods;
  if (values.length < 1) {
    throw `no scoring periods found with id '${eventId}'`;
  }
  return values;
}
