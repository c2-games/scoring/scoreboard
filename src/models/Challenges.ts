import type { EventConfiguration } from '@models/EventConfigurations';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import { Gql } from '../db';
import type { QueryOptions } from '../db';
import type { StorageFile } from '@models/Storage';
import type { Team } from '@models/Team';
import type { ChallengeSolve, ChallengeSolvesReturn } from '@models/ChallengeSolve';
import type { KanbanTask } from './Kanban';

dayjs.extend(utc);
dayjs.extend(timezone);

export type Challenge = {
  id?: string;
  name: string;
  category: string;
  description: string;
  designer: string;

  active: ActiveChallenge[];
  kanban_tasks: KanbanTask[];
};

export type ChallengeFile = {
  file_id: string;
  event_id: string;
  challenge_id: string;

  file?: StorageFile;
  event?: EventConfiguration;
  challenge?: Challenge;
};

export type ActiveChallenge = {
  points: number;
  release_time: string | null;
  released: boolean;

  event_id: string;
  challenge_id: string;

  event?: EventConfiguration;
  challenge?: Challenge;
  flags?: Flag[];
  files?: ChallengeFile[];
  solves?: ChallengeSolve[];
};

export type ChallengeWithSolves = {
  solves: ChallengeSolve[];
  id: string;
  name: string;
  category: string;
  description: string;
  designer: string;
  active: ActiveChallenge[];
};

export type ChallengeSubmission = {
  event_id: string;
  team_id: string;
  team?: Team;
  challenge_id?: string;
  challenge?: Challenge;
  flag: string;
  submitter_id: string;
  time: string;
};

export type Flag = {
  flag: string;

  event_id: string;
  challenge_id: string;
  challenge?: Challenge;
};

export type SubmitFlagParameters = {
  flag: string;
  team: Team;
  challenge: ActiveChallenge;
  submitter_id: string;
  queryOptions?: QueryOptions;
};

export async function SubmitFlag({
  flag,
  team,
  challenge,
  submitter_id,
  queryOptions,
}: SubmitFlagParameters): Promise<ChallengeSolve[]> {
  const submissionQuery = `mutation {
    insert_challenges_submissions_one(object: {
      challenge_id: ${JSON.stringify(challenge.challenge_id || challenge.challenge?.id)},
      event_id: ${JSON.stringify(challenge.event_id || challenge.event?.id)},
      flag: ${JSON.stringify(flag)},
      team_id: ${JSON.stringify(team.id)},
      submitter_id: ${JSON.stringify(submitter_id || team.name)}
    }) {
      challenge_id
      event_id
      team_id
    }
  }`;

  // Do submission
  await Gql.query(submissionQuery, queryOptions);

  // now check if we solved it
  const solveCheckQuery = `query {
  challenges_solves(where: {
    team_id: {_eq: ${JSON.stringify(team.id)}},
    challenge_id: {_eq: ${JSON.stringify(challenge.challenge_id || challenge.challenge?.id)}},
    event_id: {_eq: ${JSON.stringify(challenge.event_id || challenge.event?.id)}}
  }) {
    challenge_id
    event_id
    team_id
    time
  }}`;
  return (await Gql.query<ChallengeSolvesReturn>(solveCheckQuery, queryOptions)).challenges_solves;
}

export function UpsertChallengeQuery(challenge: Challenge): string {
  // JSON.stringify will make sure the strings have quotes around them,
  // and any extras quotes are escaped
  return `mutation {
    insert_challenges_challenges_one(
      object: {
        ${challenge.id ? 'id: ' + JSON.stringify(challenge.id) : ''}
        name:  ${JSON.stringify(challenge.name)},
        category: ${JSON.stringify(challenge.category)},
        designer: ${JSON.stringify(challenge.designer)},
        description: """${challenge.description}""",
      },
      on_conflict: {constraint: challenges_pkey, update_columns: [name category designer description]}
    ){
      id
    }
  }`;
}

export function UpdateChallengeQuery(challenge: Challenge): string {
  return `mutation {
      update_ctf_challenges(
        where: {
          id: {_eq: ${JSON.stringify(challenge.id)}}
        },
        _set: {
          name:  ${JSON.stringify(challenge.name)},
          category: ${JSON.stringify(challenge.category)},
          designer: ${JSON.stringify(challenge.designer)},
          description: """${challenge.description}""",
        }
      ){
        affected_rows
      }
  }`;
}

const ActiveChallengeQueryFields = `
  event_id
  challenge_id
  event{
    id
    name
    start
    end
  }
  points
  release_time
  released
  files {
    file_id
    event_id
    file {
      id
      createdAt
      updatedAt
      name
      size
      mimeType
    }
  }
  flags {
    flag
    event_id
  }`;

// Challenge listing for admin pages
export function ChallengesQuery(eventId: string) {
  return `{
  challenges_challenges (order_by: {category: asc, name: asc}) {
    id
    name
    designer
    description
    category
    active (order_by: {points: desc}, where: {event_id: {_eq: ${JSON.stringify(eventId)}}}) {
      ${ActiveChallengeQueryFields}
    }
  }
}`;
}

export const ChallengeWithSolvesQuery = (eventId: string) => {
  return `{
  challenges_challenges (order_by: {category: asc, name: asc}) {
    category
    description
    designer
    id
    name
    active (where: {event_id: {_eq: "${eventId}"}}) {
      ${ActiveChallengeQueryFields}
    }
    solves(where: {event_id: {_eq: "${eventId}"}}, order_by: {time: asc}) {
      challenge_id
      event_id
      submitter_id
      team_id
      time
      team {
        name
        id
      }
    }
  }
}
`;
};

export const ChallengeSubmissions = (eventId: string, limit = 20) => {
  return `{
  challenges_submissions(
    where: {event_id: {_eq: "${eventId}"}},
    order_by: {time: desc},
    ${limit ? `limit: ${limit}` : ''}
  ) {
    flag
    submitter_id
    time
    challenge_id
    event_id
    team_id
    challenge {
      id
      name
      category
      description
      designer
      active {
        ${ActiveChallengeQueryFields}
      }
    }
    team {
      id
      name
    }
  }
}
`;
};

// Challenge listing for general consumption
export function ActiveChallengesQuery({ eventId, challengeId }: { eventId?: string; challengeId?: string }): string {
  let query = '';
  if (eventId || challengeId) {
    query = 'where: {';
    if (eventId) query += `event_id: { _eq: ${JSON.stringify(eventId)} }`;
    if (challengeId) query += `challenge_id: { _eq: ${JSON.stringify(challengeId)} }`;
    query += '}';
  }
  return `{ challenges_active(${query}) { ${ActiveChallengeQueryFields} } }`;
}

export async function UpsertActiveChallenge(
  challenge: ActiveChallenge,
  queryOptions: QueryOptions = {},
  defaults?: { challengeId?: string; eventId?: string },
): Promise<boolean> {
  const query = `mutation {
    insert_challenges_active(
      objects: {
        points: ${JSON.stringify(challenge.points)}
        release_time: ${JSON.stringify(challenge.release_time || null)}
        event_id: ${JSON.stringify(challenge.event_id || defaults?.eventId)}
        challenge_id: ${JSON.stringify(challenge.challenge_id || defaults?.challengeId)}
      },
      on_conflict: {constraint: active_pkey, update_columns: [points, release_time]}
    ){
      affected_rows
    }
  }`;
  return await Gql.tryQuery(query, queryOptions);
}

export async function UpsertFlag(
  flag: Flag,
  queryOptions: QueryOptions = {},
  defaults?: { challengeId?: string; eventId?: string },
): Promise<boolean> {
  if (flag.flag.length === 0) {
    console.warn('cowardly refusing to insert empty string as a flag to the database');
    return false;
  }
  const query = `mutation {
    insert_challenges_flags(
      objects: {
        flag: ${JSON.stringify(flag.flag)}
        event_id: ${JSON.stringify(flag.event_id || defaults?.eventId)}
        challenge_id: ${JSON.stringify(flag.challenge_id || defaults?.challengeId)}
      },
      on_conflict: {constraint: flags_pkey, update_columns: [flag]}
    ){
      affected_rows
    }
  }`;
  return await Gql.tryQuery(query, queryOptions);
}

export async function UpsertFile(
  file: ChallengeFile,
  queryOptions: QueryOptions = {},
  defaults?: { challengeId?: string; eventId?: string },
): Promise<boolean> {
  const query = `mutation {
    insert_challenges_files(
      objects: {
        file_id: ${JSON.stringify(file.file_id)}
        event_id: ${JSON.stringify(file.event_id || defaults?.eventId)}
        challenge_id: ${JSON.stringify(file.challenge_id || defaults?.challengeId)}
      },
      on_conflict: {constraint: files_pkey, update_columns: [file_id]}
    ){
      affected_rows
    }
  }`;
  return await Gql.tryQuery(query, queryOptions);
}

export type UpdateChallengeResponse = {
  update_challenges_challenges: {
    affected_rows: number;
  };
};

export type InsertChallengeResponse = {
  insert_challenges_challenges: {
    affected_rows: number;
  };
};
export type DeleteChallengeResponse = {
  delete_challenges_challenges: {
    affected_rows: number;
  };
};

export type ChallengeReturn = {
  challenges_challenges: Array<Challenge>;
};

export type ActiveChallengeReturn = {
  challenges_active: Array<ActiveChallenge>;
};
