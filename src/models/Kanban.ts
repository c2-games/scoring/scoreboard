import type { ActiveChallenge } from './Challenges';
import type { ServiceStatus } from './Service';
import type { TeamUser } from './Team';
import { Gql } from '../db';
import { KanbanTasksByTeam$result } from '$houdini';

export const taskOptions: TaskStatusOption[] = [
  { status: 'Not Started', headerColor: 'success' },
  { status: 'In Progress', headerColor: 'primary' },
  { status: 'Completed', headerColor: 'secondary' },
];

export type KanbanTask =
  | KanbanTasksByTeam$result['scoreboard_kanban'][0]
  | {
      id?: string;
      event_id: string;
      check_id?: string;
      ctf_challenge_id?: string;
      team_id?: string;
      assignee?: TeamUser;
      assignee_id?: string;
      title: string;
      description: string;
      status: string;
      created_by: string;
      created_by_user?: TeamUser;
      created_at?: string;
      ctf_challenge?: ActiveChallenge;
      service_status?: ServiceStatus;
      comments?: KanbanComment[];
    };

export type TaskStatusOption = { status: string; headerColor: string };

export type KanbanComment = {
  id: string;
  author_id: string;
  author?: TeamUser;
  content: string;
  created: string;
  modified: string;
  task_id: string;
  task?: KanbanTask;
};

export const KanbanCommentFields = `
    id
    author_id
    content
    created
    modified
    task_id
`;

export const KanbanFields = `
    assignee_id
    check_id
    created_at
    description
    id
    status
    title
    created_by
    ctf_challenge {
      challenge {
        name
        description
      }
    }
    comments {
      id
      author_id
      content
      created
      modified
    }
    `;

export const KanbanQuery = `{
   scoreboard_kanban {
    ${KanbanFields}
  }
}`;

export const KanbanQueryByChallenge = (challenge_id: string) => `{
   scoreboard_kanban(where: {ctf_challenge_id: {_eq: ${JSON.stringify(challenge_id)} }}) {
    ${KanbanFields}
  }
}`;

export async function createKanbanTask(
  // temp typing to force required fields
  task: KanbanTask & { team_id: string; event_id: string; ctf_challenge_id: string },
): Promise<{ id: string }> {
  return await Gql.query(`mutation CreateKanbanTask {
      insert_scoreboard_kanban_one(object: {
        assignee_id: ${JSON.stringify(task.assignee_id || null)},
        created_by: ${JSON.stringify(task.created_by)}, 
        title: ${JSON.stringify(task.title)}, 
        description: ${JSON.stringify(task.description)}, 
        status: ${JSON.stringify(task.status)}, 
        team_id: ${JSON.stringify(task.team_id)}, 
        event_id: ${JSON.stringify(task.event_id)},
        ctf_challenge_id: ${JSON.stringify(task.ctf_challenge_id || null)},
        check_id: ${JSON.stringify(task.check_id || null)},
        created_at: ${JSON.stringify(task.created_at || new Date().toISOString())},
      }) {
        ${KanbanFields}
      }
    }`);
}

export async function updateTaskStatus(taskId: string, status: string | null) {
  await Gql.query(`mutation UpdateKanbanAssignee {
      update_scoreboard_kanban_by_pk(
        pk_columns: { id: ${JSON.stringify(taskId)} },
        _set: { status: ${JSON.stringify(status)} }
      ) {
        id
      }
    }`);
}

export async function updateTaskAssignee(taskId: string, assignee: string | null) {
  await Gql.query(`mutation UpdateKanbanAssignee {
      update_scoreboard_kanban_by_pk(
        pk_columns: { id: ${JSON.stringify(taskId)} },
        _set: { assignee_id: ${JSON.stringify(assignee)} }
      ) {
        id
      }
    }`);
}

export async function updateTaskDescription(taskId: string, description: string) {
  await Gql.query(`mutation UpdateKanbanDescription {
      update_scoreboard_kanban_by_pk(
        pk_columns: { id: ${JSON.stringify(taskId)} },
        _set: { description: ${JSON.stringify(description)} }
      ) {
        id
      }
    }`);
}

export async function updateTaskTitle(taskId: string, title: string) {
  await Gql.query(`mutation UpdateKanbanTitle {
      update_scoreboard_kanban_by_pk(
        pk_columns: { id: ${JSON.stringify(taskId)} },
        _set: { title: ${JSON.stringify(title)} }
      ) {
        id
      }
    }`);
}

export async function deleteKanbanTask(taskId: string): Promise<{ id: string }> {
  return await Gql.query(`mutation {
      delete_scoreboard_kanban(where: {id: {_eq: ${JSON.stringify(taskId)}}}) { affected_rows }
    }`);
}

export async function createKanbanComment(comment: Partial<KanbanComment>): Promise<{ id: string }> {
  return await Gql.query(`mutation CreateKanbanComment {
      insert_scoreboard_kanban_comments_one(object: {
        author_id: ${JSON.stringify(comment.author_id)},
        content: ${JSON.stringify(comment.content)}, 
        created: ${JSON.stringify(comment.created || new Date().toISOString())},
        modified: ${JSON.stringify(comment.modified || new Date().toISOString())}, 
        task_id: ${JSON.stringify(comment.task_id)}, 
      }) {
        ${KanbanCommentFields}
      }
    }`);
}

export async function updateKanbanComment(commentId: string, content: string, at?: string) {
  await Gql.query(`mutation UpdateKanbanComment {
      update_scoreboard_kanban_comments_by_pk(
        pk_columns: { id: ${JSON.stringify(commentId)} },
        _set: { content: ${JSON.stringify(content)}, modified: ${JSON.stringify(at || new Date().toISOString())} }
      ) {
        id
      }
    }`);
}

export async function deleteKanbanComment(commentId: string): Promise<{ id: string }> {
  return await Gql.query(`mutation {
      delete_scoreboard_kanban_comments(where: {id: {_eq: ${JSON.stringify(commentId)}}}) { affected_rows }
    }`);
}
