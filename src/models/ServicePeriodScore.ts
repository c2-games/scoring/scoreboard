import type { Team } from '@models/Team';
import type { Check } from '@models/Service';

// todo finish filling out
export type ServicePeriodScore = {
  team?: Team;
  total_team_checks: number;
  check?: Check;
  max_checks: number;
  max_points: number;
  score: number;
  successful_team_checks: number;
  period?: {
    start: string;
    end: string;
  };
};
