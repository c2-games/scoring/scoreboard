import type { EventConfiguration } from '@models/EventConfigurations';
import type { Team } from '@models/Team';
import type { Challenge } from '@models/Challenges';

export type ChallengeSolve = {
  submitter_id: string;
  time: string;

  challenge_id: string;
  team_id: string;
  event_id: string;

  challenge?: Challenge;
  event?: EventConfiguration;
  team?: Team;
};

export type ChallengeSolvesReturn = {
  challenges_solves: ChallengeSolve[];
};

export function GetSolvedChallengesByTeam(teamId: string): string {
  return `{
    challenges_solves(where: {team_id: {_eq: ${teamId}}}) {
      challenge_id
      submitter_id
      time
      team_id
    }
  }`;
}
