/**
 * Configurations
 */
export type PveConfig = {
  url: string;
  pool: string;
  node: string;
};

/**
 * Bundles
 */
export enum BundleCategory {
  WorldOfBills = 'world-of-bills',
  PacketManPing = 'pmp',
  MiniHack = 'mini-hack',
  CTF = 'ctf',
}

export const BundleCategoryNames = {
  [BundleCategory.WorldOfBills]: 'World of Bills',
  [BundleCategory.PacketManPing]: 'Packet Man Ping',
  [BundleCategory.MiniHack]: 'Mini Hack',
  [BundleCategory.CTF]: 'Capture the Flag',
};

export type Resource = {
  order: number;
  title: string;
  url: string;
};

export type Bundle = {
  id: string;
  name: string;
  description?: string;
  how_to_play?: string;
  resources?: Resource[];
  templates: TemplateAssociation[];
  // new api fields
  created: string;
  modified: string;
  active: boolean;
  // legacy: boolean;
  // memory: number | undefined;
  // cpu_count: number | undefined;
  // storage: number | undefined;
};

export type TemplateAssociation = {
  template_id: string;
  user_role: 'NoAccess' | 'PVEVMUser';
};

/**
 * Templates
 */
export enum TemplateType {
  qemu = 'QEMU',
  lxc = 'LXC',
  k8 = 'K8',
}

export enum TemplateState {
  valid = 'VALID',
  error = 'ERROR',
  init = 'INITIALIZING',
}

export type Template = {
  id: string;
  name: string;
  description: string;
  // old api fields
  vmid: string;

  // new api fields
  // foreign_id: string;
  // type: TemplateType;
  // created: string;
  // modified: string;
  // memory: number | undefined;
  // cpu_count: number | undefined;
  // storage: number | undefined;
  // state: TemplateState;
};

/**
 * Environments
 */
export enum EnvironmentState {
  running = 'RUNNING',
  stopped = 'STOPPED',
  creating = 'CREATING',
  deleting = 'DELETING',
  error = 'ERROR',
  // new API states (not used in old API):
  init = 'INITIALIZING',
  starting = 'STARTING',
  stopping = 'STOPPING',
}

export function environmentIsRunning(environment: Environment) {
  return [EnvironmentState.running, EnvironmentState.stopping].includes(environment?.state);
}

export type Environment = {
  id: string;
  owner: string;
  state: EnvironmentState;
  // old API fields
  bundle: string; // bundle id
  vmids: number[];
  // new api fields
  created: string;
  modified: string;
  // deleted: boolean;
  // qemu_ids: number[];
  // name: string;
  // description: string | undefined;
  // bundle_id: string;
};
