import type { BundleCategory } from '@models/Sandbox';
import type { OidcManager } from '@auth/keycloak';

type PageParameters = {
  params: { category: BundleCategory };
  context: { keycloak: OidcManager };
};

export async function load({ params }: PageParameters) {
  return {
    category: params.category,
  };
}
