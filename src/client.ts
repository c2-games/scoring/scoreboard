import { HoudiniClient, subscription } from '$houdini';
import { createClient } from 'graphql-ws';
import { error } from '@sveltejs/kit';
import Env from '@env';

export default new HoudiniClient({
  url: Env.GqlPublicUri,
  fetchParams({ session, metadata }) {
    const headers: Record<string, string> = {};

    if (metadata?.role) {
      headers['x-hasura-role'] = metadata.role;
    }

    if (session?.token) {
      headers.Authorization = `Bearer ${session?.token}`;
    }

    return { headers };
  },
  plugins: [
    subscription((ctx) =>
      createClient({
        url: Env.GqlPublicUri.replace('http', 'ws'),
        connectionParams: () => {
          console.debug('setting connectionParams for subscription', ctx);
          return {
            headers: ctx.fetchParams?.headers,
          };
        },
      }),
    ),
  ],
  throwOnError: {
    operations: ['all'],
    // the function to call
    error: (errors, ctx) => error(500, `(${ctx.artifact.name}): ` + errors.map((err) => err.message).join('. ') + '.'),
  },
});
