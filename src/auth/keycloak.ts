import Env from '../env';
import { EventRole } from '@auth/models';
import {
  Log,
  User,
  UserManager,
  type UserProfile,
  Logger,
  type UserManagerSettings,
  type INavigator,
} from 'oidc-client-ts';
import { get, writable, type Writable } from 'svelte/store';
import { page } from '$app/stores';
import { browser } from '$app/environment';
import { UserTeamByEventIdStore, setClientSession } from '$houdini';
import { goto } from '$app/navigation';
import { eventId, keycloak, team } from '@stores';

export type C2GamesProfile = UserProfile & {
  // These fields are known to be populated from Keycloak
  given_name: string;
  family_name: string;
  name: string;
  email: string;
  email_verified: boolean;
  preferred_username: string;
  'https://hasura.io/jwt/claims': {
    'x-hasura-default-role': EventRole;
    'x-hasura-allowed-roles': EventRole[];
    'x-hasura-user-email': string;
    'x-hasura-user-id': string;
  };
  'https://c2games.org/jwt/claims': {
    'x-hackathon-event-roles': EventRole[];
  };
  exp: number;
};

export type C2GamesUser = User & { profile: C2GamesProfile };

export function getHasuraRolesFromProfile(profile?: C2GamesProfile): EventRole[] | null {
  return profile?.['https://hasura.io/jwt/claims']?.['x-hasura-allowed-roles'] || null;
}

export class OidcManager extends UserManager {
  // store current user as UserManager.getUser() is async
  _currentUser: Writable<C2GamesUser | null> = writable(null);
  // whether we have successfully attempted to signinSilent()
  initialized: boolean = false;
  // set up our own logger. This will use the same log level as all oidc-client-ts classes
  protected readonly logger = new Logger('OidcManager');

  get currentUser(): C2GamesUser | null {
    return get(this._currentUser);
  }

  set currentUser(user: C2GamesUser | null) {
    this._currentUser.set(user);
  }

  constructor(
    settings: UserManagerSettings,
    redirectNavigator?: INavigator,
    popupNavigator?: INavigator,
    iframeNavigator?: INavigator,
  ) {
    // initialize the base class
    super(settings, redirectNavigator, popupNavigator, iframeNavigator);

    this.enableLogging(Env.OidcLogLevel);
    this.logger.debug('oidc settings', settings);

    this.events.addUserLoaded(async (user: User) => {
      // update the token cookie. This needs to be done by the server so it's an HTTP-only cookie.
      // We use a callback with void because the event manager will actually wait for these to complete,
      // but we want them to happen in the background and not hold up any page loads
      const token = user.access_token;
      setClientSession({ token, roles: getHasuraRolesFromProfile((user as C2GamesUser).profile) });
      this.currentUser = user as C2GamesUser;

      try {
        const store = new UserTeamByEventIdStore();
        const variables = { eventId: get(eventId), userId: user.profile.sub };
        console.debug('fetching team for user', variables);
        const result = await store.fetch({ variables });
        console.debug('team result', result);
        team.set(result.data?.teams[0]);
      } catch (err) {
        console.warn('failed to get team by id', err);
      }
    });

    // update the token cookie. This needs to be done by the server so it's an HTTP-only cookie.
    // We use a callback with void because the event manager will actually wait for these to complete,
    // but we want them to happen in the background and not hold up any page loads

    // handle a token expiring. It will automatically refresh under normal conditions,
    // but some scenarios like private browsers won't allow the silent refresh to function.
    this.events.addUserUnloaded(async () => {
      this.currentUser = null;
      team.set(undefined);
      setClientSession({ token: null, roles: null });

      const url = get(page).url;
      if (!url) {
        this.logger.error('user was unloaded, and page.url is undefined !?!?!??!');
        return;
      }
      this.logger.debug(`user unloaded, checking if authentication is required for page ${url.pathname}`);
    });

    this.events.addAccessTokenExpired(async () => {
      this.logger.error('oidc: access token expired, unloading user');
      // remove user, which will trigger other events as appropriate
      await this.events.unload();
    });

    console.debug('OidcManager', this);
  }

  async login(url_state: string) {
    this.logger.debug(`redirecting to signinRedirect path=${get(page).url?.pathname} url_state=${url_state}`);
    await this.signinRedirect({ url_state });
  }

  async logout() {
    await goto('/');
    await this.signoutRedirect({ post_logout_redirect_uri: window.location.href });
  }

  async initialize(): Promise<User | null> {
    /**
     * Try to load user from:
     * 1. From session storage
     * 2. A silent signin flow
     */
    // if already initialized, we've got what we've got
    if (this.initialized) return this.currentUser;

    try {
      // attempt to load the current user from session storage
      const user = (await this.getUser()) as User;

      // did we get a user, and is the JWT expired?
      if (user) {
        this.logger.debug('found existing user JWT in session storage', user);
        const now = new Date().getTime() / 1000;
        if (user.profile.exp > now) {
          // force a userLoaded event, because the library doesn't do it in getUser()
          // note: this actually waits for all event handlers before returning
          await this.events.load(user);
          this.initialized = true;
          return user as User;
        }

        this.logger.debug('existing JWT is expired');
      }
    } catch (e) {
      this.logger.warn('failed to load oidc user', e);
    }

    try {
      // no valid token available, start a silent signin flow
      this.logger.debug('starting silent signin');
      return (await this.signinSilent()) as User | null;
    } catch {
      // we should be able to ignore errors from silent signin
      return null;
    } finally {
      this.logger.debug(`initialization complete authenticated=${this.authenticated}`);
      // finally set initialized to true, whether we have a user or not - at least we tried.
      this.initialized = true;
    }
  }

  enableLogging(oidcLogLevel: Log) {
    Log.setLogger({
      debug: console.debug,
      info: console.info,
      warn: console.warn,
      error: console.error,
    });
    Log.setLevel(oidcLogLevel);
    this.logger.info('log level set to', oidcLogLevel);
  }

  /*
   * Convenience Getters
   */
  public get authenticated(): boolean {
    return !!this.currentUser;
  }

  public get token(): string | undefined {
    return this.currentUser?.access_token;
  }

  public get tokenParsed(): UserProfile | undefined {
    return this.currentUser?.profile;
  }

  public get userId(): string | undefined {
    return this.currentUser?.profile.sub;
  }

  /**
   * Try to retrieve user email from a known field
   */
  public get userEmail(): string | undefined {
    return this.currentUser ? this._extractUserEmail(this.currentUser.profile) : undefined;
  }

  private _extractUserEmail(profile: C2GamesProfile): string | undefined {
    return profile['https://hasura.io/jwt/claims']?.['x-hasura-user-email'] || profile.email;
  }

  public get hasuraRoles(): EventRole[] | null {
    return getHasuraRolesFromProfile(this.currentUser?.profile);
  }

  public get hasuraDefaultRole(): EventRole | undefined {
    return this.currentUser?.profile['https://hasura.io/jwt/claims']?.['x-hasura-default-role'];
  }

  isRole(role: EventRole | string): boolean {
    return this.hasuraRoles?.includes(role as EventRole) || false;
  }

  getFirstRole(roles: EventRole[]): EventRole | null {
    for (const role of roles) {
      if (this.isRole(role)) return role;
    }
    return null;
  }

  isStaff(): boolean {
    /**
     * Check if user has a staff role
     */
    const roles = [EventRole.EventAdmin, EventRole.CTFAdmin, EventRole.Attacker, EventRole.Staff];
    return !!this.getFirstRole(roles);
  }

  getHighestPrivilegeRole(): EventRole | undefined {
    /**
     * DEPRECATED: Find the highest privilege role in the user's profile.
     */
    const roles = [
      EventRole.SuperAdmin,
      EventRole.EventAdmin,
      EventRole.CTFAdmin,
      EventRole.Attacker,
      EventRole.Staff,
      EventRole.Participant,
    ];
    return this.getFirstRole(roles) || this.hasuraDefaultRole;
  }
}

const ssoRedirectPath = '/ssoRedirect';
const ssoRedirect = browser ? window.origin + ssoRedirectPath : '';
export const oidcSettings: UserManagerSettings = {
  authority: Env.OidcAuthority,
  client_id: Env.OidcClientId,
  silentRequestTimeoutInSeconds: 4,
  response_type: 'code',
  // scope: 'openid profile email hasura',
  filterProtocolClaims: true,
  redirect_uri: ssoRedirect,
  silent_redirect_uri: ssoRedirect,
};

// only create class if we're in the browser
const singleton = browser ? new OidcManager(oidcSettings) : ({} as OidcManager);
// LEGACY: set the keycloak store. Future use should `import oidc from '@auth/keycloak'`
keycloak.set(singleton);
export default singleton;
