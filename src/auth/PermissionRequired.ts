import { keycloak, team, eventId } from '@stores';
import { goto } from '$app/navigation';
import { EventRole, type NullableEventRole } from './models';
import { Gql } from '@db';
import { EventCfgByIdQuery } from '@models/EventConfigurations';
import type { EventConfiguration } from '@models/EventConfigurations';
import { get } from 'svelte/store';

export function redirectToUnauthorized(): Promise<void> {
  return goto(`/error/unauthorized?referer=${window.location.pathname}`);
}

export async function requireAuth() {
  const oidc = get(keycloak);
  if (!oidc.authenticated) await oidc.login(window.location.pathname);
}

export async function requireEventStarted() {
  const oidc = get(keycloak);
  if (oidc.isStaff()) return;

  const event = (await Gql.query<{ events_by_pk: EventConfiguration }>(EventCfgByIdQuery(get(eventId)))).events_by_pk;
  if (!event?.start) {
    console.warn('event start date not found, requireEventStarted is not redirecting');
    return;
  }

  if (!event.started) {
    console.warn('event is not open, redirecting to login!');
    await goto('/countdown');
  }
}

export async function requireTeam() {
  await requireAuth();
  const oidc = get(keycloak);
  if (oidc.isStaff()) return;

  if (!oidc.isRole(EventRole.Participant)) {
    console.warn('user is not a participant, redirecting unauthorized!');
    return goto(`/error/unauthorized?referer=${window.location.pathname}`);
  }

  if (!get(team)) {
    console.warn('user does not have team, redirecting unauthorized!');
    return goto(`/error/unauthorized?referer=${window.location.pathname}`);
  }
}

/**
 * Require authentication and a particular role to load the page
 * @param roleOrElement Request Role OR HTMLElement requiring the auth. Element is not used,
 *                      only provided for svelte 'use:' compatibility. Will be remapped to required Role if a string.
 * @param requiredRole Role required if element is specified as first parameter
 */
export async function requireRole(
  roleOrElement: HTMLElement | EventRole,
  requiredRole: NullableEventRole = null,
): Promise<void> {
  console.log(roleOrElement, requiredRole);
  if (typeof roleOrElement === 'string') requiredRole = roleOrElement;
  // First of all, auth is required (and requireAuth watches for changes to auth)
  await requireAuth();
  // Then check for role
  if (!requiredRole || !get(keycloak).isRole(requiredRole)) {
    await redirectToUnauthorized();
  }
}

export default requireRole;
