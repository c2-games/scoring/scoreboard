import houdini from 'houdini/vite';
import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';

const config: UserConfig = {
  plugins: [houdini(), sveltekit()],
  server: {
    fs: {
      allow: ['static'],
    },
  },
};

export default config;
