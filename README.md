# Cyboard

**Cyboard** is a scoring engine for the cyber defense competition
[CNY Hackathon](https://www.cnyhackathon.org 'CNY Hackathon Home').

<!-- Generated with https://github.com/jonschlinkert/markdown-toc -->
<!-- toc -->

- [Cyboard](#cyboard)
  - [Background](#background)
  - [Features](#features)
  - [Building](#building)
  - [Development](#development)
    - [Editor](#editor)
    - [Running the App](#running-the-app)
      - [Common Steps](#common-steps)
      - [Running UI with Docker](#running-ui-with-docker)
      - [Running UI locally](#running-ui-locally)
    - [Redeploying after changes](#redeploying-after-changes)
    - [Code Quality Control](#code-quality-control)
      - [ESLint](#eslint)
      - [Prettier](#prettier)
  - [Administration](#administration)
    - [CTF File Uploads](#ctf-file-uploads)
    - [Users and Roles](#users-and-roles)

<!-- tocstop -->

## Background

CNY Hackathon is a joint cyber security defense & CTF event for intercollegiate
students in the US North East region. The event is hosted bi-annually to
100+ contestants. Despite the name, it shares no similarities with a
[programming hackathon](https://en.wikipedia.org/wiki/Hackathon).

[Tony](https://github.com/pereztr5 'Tony Perez') first developed Cyboard in 2016
as his senior project at SUNY Polytechnic. Ever since Tony's graduation,
[Butters](https://github.com/tbutts 'Tyler Butters') stepped up as the project's
developer.
TODO: Some notes about the updates since CAE

## Features

## Building

Use Docker to build the production image:

```bash
docker build .
```

## Development

### Editor

Recommended editors are [Webstorm](https://www.jetbrains.com/webstorm/) or [VS Code](https://code.visualstudio.com/).

Whatever editor you choose, please configure it to:

- Automatically apply [prettier](https://prettier.io/) formatting on save
- Support Typescript, Javascript, and Svelte
- Display ESLint errors _based on the project linting configuration_

### Running the App

There are two ways to run the scoreboard UI - Via Docker or locally using npm.

#### Common Steps

Before firing up the scoreboard application:

1. Start Hasura from it's dedicated repository: https://gitlab.com/c2-games/scoring/hasura
   - This will start Hasura on the `c2games` Docker Network, and expose Hasura on http://localhost:8080
   - Note: If using a cloud-based Hasura instance, skip this step
2. If running in Sandbox mode, start the Sandbox API server.
   - If you're unsure if you need sandbox mode, you don't :)
3. Configure your environment with the `.env` file

   - The following options are available:
     - `VITE_GQL_PUBLIC_URI`: URI of GraphQL, usually localhost:8080. This is overridden by docker-compose when using docker
     - `VITE_SANDBOX_API_URI`: URI of the Sandbox API. This is only needed if running in Sandbox Mode.
     - `VITE_ASSET_STORAGE_URI`: URI of Static Asset Storage.
     - `VITE_OIDC_AUTHORITY`: URI of OIDC provider - this will be the URL just above `.well-known/openid-configuration`.
       - Example, `https://auth.ncaecybergames.org/realms/prod`
     - `VITE_OIDC_CLIENT_ID`: Client ID within OIDC provider
     - `VITE_OIDC_DEBUG`: Specify any value to increase the OIDC library logging level to `debug`.
     - `VITE_EVENT_ID`: Event that this scoreboard instance is displaying data for. Does not generally affect staff roles.
     - `VITE_STORAGE_API`: API URL for Hasura Storage Service. Must be base URL, /v1/\* will be appended.
     - `VITE_STORAGE_BUCKET`: Bucket used for file storage within S3 server.
     - `VITE_STORAGE_S3_API`: (Optional) API URL for S3 server - used to generate S3 links directly.
       - If not specified, VITE_STORAGE_API will be used. Not recommended for local development.
     - `VITE_TAPI_URI`: (Optional) API URL for the c2games tracking API
       - Used to import events from the tracking API into the competition GraphQL
     - `VITE_INTERCOM_WARNING`: (Optional) Warning message to display in Intercom. Useful when system is broken.
     - `VITE_INTERCOM_ERROR`: (Optional) Error message to display in Intercom. Useful when system is broken.
   - Example .env file:

     ```dotenv
     VITE_GQL_PUBLIC_URI=http://localhost:8080/v1/graphql
     VITE_SANDBOX_API_URI=http://127.0.0.1:8000/
     VITE_ASSET_STORAGE_URI=
     VITE_KEYCLOAK_URL=https://auth.c2games.org
     VITE_KEYCLOAK_REALM=dev
     VITE_KEYCLOAK_CLIENT_ID=scoreboard-ui
     VITE_EVENT_ID=7803635d-419f-420d-a426-0f27b8c1e08b
     VITE_STORAGE_API=http://localhost:8181
     VITE_STORAGE_BUCKET=default
     VITE_TAPI_URI=https://devapi.ncaecybergames.org
     #VITE_INTERCOM_WARNING="System is currently offline for maintenance"
     #VITE_INTERCOM_ERROR="System is currently offline for maintenance"
     ```

#### Running UI with Docker

The first time you run docker, you will need to execute the script
`setup/nginx/generate_localhost_ssl_certs.sh` to generate SSL keys.

To start the application with docker, simple run `docker-compose up` and access the app on `https://localhost`.

#### Running UI locally

The application can also be run locally using npm:

```bash
# Install dependencies
npm install
# Run the development server
npm run dev
```

### Redeploying after changes

Changes to the UI should be automatically reloaded.

### Code Quality Control

Currently, two tools are in use for code quality: Prettier and ESLint. Both are expected to be passing for all contributions.

To run both tools, use the command `npm run lint`

To attempt any automatic fixes available, such as applying prettier formatting or eslint auto-fixes, use the command:

```bash
npm run lint:fix
```

#### ESLint

To run only ESLint checks, use the command `npm run lint:eslint`.

#### Prettier

To run only Prettier checks, use the command `npm run lint:format`.

## Administration

### CTF File Uploads

TODO: Talk about how the `data` directory is stored on a docker volume for persistence.

### Users and Roles

TODO: Need to figure out how best to do keycloak still

## Attribution & Thanks

- CC SVG files
  - https://www.svgrepo.com/svg/532236/crosshair
  - https://www.svgrepo.com/svg/533133/monitor-alt-2
