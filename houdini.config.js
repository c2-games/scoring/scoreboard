/// <references types="houdini-svelte">

import { resolvers as scalarResolvers } from 'graphql-scalars';

/** @type {import('houdini').ConfigFile} */
const config = {
  // watchSchema: {
  //   url: 'http://localhost:8080/v1/graphql'
  // },
  plugins: {
    'houdini-svelte': {},
  },
  features: {
    imperativeCache: true,
  },
  defaultFragmentMasking: 'disable',
  types: {
    // This key specification is so that houdini can properly identify
    // unique results in this table (this is the pkey)
    wrapup_snapshots: {
      keys: ['id', 'event_id', 'team_id'],
    },
  },
  scalars: {
    uuid: {
      // the corresponding typescript type
      type: scalarResolvers['UUID'].name,
      // turn the api's response into that type
      unmarshal(val) {
        return scalarResolvers['UUID'].parseValue(val);
      },
      // turn the value into something the API can use
      marshal(val) {
        return scalarResolvers['UUID'].serialize(val);
      },
    },
    // the name of the scalar we are configuring
    timestamptz: {
      // the corresponding typescript type
      type: scalarResolvers['DateTime'].name,
      // turn the api's response into that type
      unmarshal(val) {
        return scalarResolvers['DateTime'].parseValue(val);
      },
      // turn the value into something the API can use
      marshal(val) {
        return scalarResolvers['DateTime'].serialize(val);
      },
    },
  },
};

export default config;
