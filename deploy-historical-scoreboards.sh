set -e

# 2025
declare -A events=(
    ["eo"]="ef65b001-1087-4330-939f-e6cc3a3cf610"
    ["wo"]="21928f90-d8e5-449d-93d6-e979f4a13961"
    ["ne1"]="6458b057-6c5b-4e94-a9ba-872a17ac78fe"
    ["mw1"]="129e49de-3a5e-492b-8e53-fdfc1fa597b0"
    ["se1"]="b62c7229-5d89-4e06-9dfb-476b421e7b0d"
    ["sw1"]="34843c7b-5cd1-4652-8831-596bed0c04d7"
    ["mw2"]="bdc13c5f-42c6-4d92-b8f5-b2582ef607c2"
    ["nw1"]="65e41adc-874f-4a91-8100-c09bc1649b20"
    ["se2"]="6424b4fd-c5ed-41f3-9fb7-81314e12fb29"
    ["nw2"]="84faf14d-62fd-4cb8-9942-36a0d9e496cb"
    ["ne2"]="38cd29b3-3dc4-44be-95ec-38359a054228"
    ["sw2"]="989df538-3db3-4322-8688-0223b93a3526"
)

for region in "${!events[@]}"; do
    export VITE_EVENT_ID="${events[$region]}"
    project="$region-scoreboard"
    echo "creating and publishing $project ($VITE_EVENT_ID)"
    npm run build > /dev/null 2>&1
    wrangler pages deploy .svelte-kit/cloudflare --project-name "$project" --branch $CI_COMMIT_BRANCH --commit-hash $CI_COMMIT_SHA
done

