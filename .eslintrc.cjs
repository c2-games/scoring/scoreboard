// ugins: ['@typescript-eslint', 'prettier'],
//   rules: {
//     'import/extensions': 'off',
//     'import/no-unresolved': 'off',
//     'import/no-mutable-exports': 'off',
//     'no-plusplus': 'off',
//     '@typescript-eslint/no-floating-promises': 'off',
//     '@typescript-eslint/restrict-template-expressions': 'off',
//     'consistent-return': 'off',
//     'no-unused-expressions': ['error', { allowTernary: true }],
//     'a11y-click-events-have-key-events': 'off',
//   },
// extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],

module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    extraFileExtensions: ['.svelte'],
    sourceType: 'module',
  },
  ignorePatterns: ['*.cjs'],
  plugins: ['@typescript-eslint', 'prettier'],
  rules: {
    'no-inner-declarations': 'off',
    'prefer-const': 'off',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'import/no-mutable-exports': 'off',
    'no-plusplus': 'off',
    '@typescript-eslint/no-floating-promises': 'off',
    '@typescript-eslint/restrict-template-expressions': 'off',
    'consistent-return': 'off',
    'no-unused-expressions': ['error', { allowTernary: true }],
    'a11y-click-events-have-key-events': 'off',
  },
  overrides: [
    {
      files: ['*.svelte'],
      parser: 'svelte-eslint-parser',
      parserOptions: {
        parser: '@typescript-eslint/parser',
      },
    },
  ],
};
